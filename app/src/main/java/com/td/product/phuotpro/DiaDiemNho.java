package com.td.product.phuotpro;

import java.util.ArrayList;

/**
 * Created by Admin on 4/24/2017.
 */

public class DiaDiemNho {

    public String title;
    public String imagelink;
    public String placeid;
    public String info;
    public double latitude;
    public double longtitude;
    public String address;
    public String hotelid;
    public ArrayList<String> listimage;

    public DiaDiemNho() {

    }

    public DiaDiemNho(String title, String imagelink, String placeid, ArrayList<String> listimage, String info, double lattitude, double longtitude, String address, String hotelid) {
        this.title = title;
        this.imagelink = imagelink;
        this.placeid = placeid;
        this.info = info;
        this.listimage = listimage;
        this.latitude = lattitude;
        this.longtitude = longtitude;
        this.address = address;
        this.hotelid = hotelid;

//        this.phuongtienden=phuongtienden;
    }


    public String getTitle() {
        return title;
    }

    public String getPlaceid() {
        return placeid;
    }

    public void setPlaceid(String placeid) {
        this.placeid = placeid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImagelink() {
        return imagelink;
    }

    public void setImagelink(String imagelink) {
        this.imagelink = imagelink;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public ArrayList<String> getListimage() {
        return listimage;
    }

    public void setListimage(ArrayList<String> listimage) {
        this.listimage = listimage;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
