package com.td.product.phuotpro.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ViewFlipper;
import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.activity.PlaceActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class TravelFragment extends Fragment {
    float x1,x2;
    View v;
    ImageView imgv1,imgv2,imgv3,imgv4,imgv5,imgv6,imgv7,imgv8;
    ViewFlipper viewFlipper;
    String [] listlink={"http://znews-photo-td.zadn.vn/w1024/Uploaded/jac_iik/2015_11_17/10_9534_1.jpg",
                        "http://znews-photo-td.zadn.vn/w1024/Uploaded/jac_iik/2015_11_17/12_DSC09348_1.jpg",
                        "http://znews-photo-td.zadn.vn/w1024/Uploaded/jac_iik/2015_11_17/16_DSC00409_1.jpg",
                        "http://znews-photo-td.zadn.vn/w1024/Uploaded/jac_iik/2015_11_17/18_hoian_1.jpg",
                        "http://znews-photo-td.zadn.vn/w1024/Uploaded/jac_iik/2015_11_17/13_DSC7084Edit2_1.jpg"};

    public TravelFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this com.td.product.phuotpro.fragment
        v= inflater.inflate(R.layout.fragment_travel, container, false);
        init();
        loadData();
        setclick();
        return  v;

    }

    private void setclick() {
        imgv1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(), PlaceActivity.class);
                i.putExtra("category","TayBac");
                startActivity(i);
            }
        });
        imgv2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(), PlaceActivity.class);
                i.putExtra("category","DongBac");
                startActivity(i);
            }
        });
        imgv3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(), PlaceActivity.class);
                i.putExtra("category","DBSongHong");
                startActivity(i);
            }
        });
        imgv4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(), PlaceActivity.class);
                i.putExtra("category","BacTrungBo");
                startActivity(i);
            }
        });
        imgv5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(), PlaceActivity.class);
                i.putExtra("category","TayNguyen");
                startActivity(i);
            }
        });
        imgv6.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(), PlaceActivity.class);
                i.putExtra("category","NamTrungBo");
                startActivity(i);
            }
        });
        imgv7.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(), PlaceActivity.class);
                i.putExtra("category","DongNamBo");
                startActivity(i);
            }
        });
        imgv8.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getActivity(), PlaceActivity.class);
                i.putExtra("category","TayNamBo");
                startActivity(i);
            }
        });
    }


    private void loadData() {
        for (int i=0;i<listlink.length;i++){
            ImageView imageView=new ImageView(getActivity());
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Picasso.with(getContext()).load(listlink[i]).into(imageView);
            viewFlipper.addView(imageView);
        }
        viewFlipper.setFlipInterval(3000);
        viewFlipper.setAutoStart(true);
        Animation animation_slide_in= AnimationUtils.loadAnimation(getActivity(),R.anim.slide_in_right);
        Animation animation_slide_out= AnimationUtils.loadAnimation(getActivity(),R.anim.slide_out_right);
        viewFlipper.setInAnimation(animation_slide_in);
        viewFlipper.setOutAnimation(animation_slide_out);
//        viewFlipper.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch (event.getAction()){
//                    case MotionEvent.ACTION_DOWN:
//                        x1=event.getX();
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        x2=event.getX();
//                        if(x2>x1){
//                            viewFlipper.showPrevious();
//                        }
//                        else {
//                            viewFlipper.showNext();
//                        }
//                        break;
//                }
//                return false;
//            }
//        });
        Picasso.with(getActivity()).load("https://www.vietravel.com/images/news/thumb-ruong-lua-tay-bac.jpg").into(imgv1);
        Picasso.with(getActivity()).load("https://travel.com.vn/Images/destination/tf_141216_71610595.jpg").into(imgv2);
        Picasso.with(getActivity()).load("http://lancorp.info/V_Lantour/admin/images/tintuc/123.jpg").into(imgv3);
        Picasso.with(getActivity()).load("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSLj-PHyCUYF7J8hsluY50Wudy-UIOdKCdKCum0VUZFBkNoHZFr").into(imgv4);
        Picasso.with(getActivity()).load("http://khachsandalat.pro/wp-content/uploads/2016/10/thanh-pho-da-lat-01.jpg").into(imgv5);
        Picasso.with(getActivity()).load("https://media.licdn.com/mpr/mpr/shrinknp_800_800/AAEAAQAAAAAAAAivAAAAJGQ3YTRjZmQ2LTM3YTYtNDJkNy04ZmQ0LWNmZGQ3NzA2MWI2MA.jpg").into(imgv6);
        Picasso.with(getActivity()).load("http://cholontourist.com.vn/uploads/images/Tour-30-4/Du-lich-can-tho/Cau-my-thuan-tour-30-4-du-lich-can-tho.jpg").into(imgv7);
        Picasso.with(getActivity()).load("http://girly.vn/wp-content/uploads/2016/06/Sai-gon-trong-toi.jpeg").into(imgv8);

    }

    private void init() {
        viewFlipper=(ViewFlipper)v.findViewById(R.id.viewflipper);
        imgv1=(ImageView)v.findViewById(R.id.imgv1);
        imgv2=(ImageView)v.findViewById(R.id.imgv2);
        imgv3=(ImageView)v.findViewById(R.id.imgv3);
        imgv4=(ImageView)v.findViewById(R.id.imgv4);
        imgv5=(ImageView)v.findViewById(R.id.imgv5);
        imgv6=(ImageView)v.findViewById(R.id.imgv6);
        imgv7=(ImageView)v.findViewById(R.id.imgv7);
        imgv8=(ImageView)v.findViewById(R.id.imgv8);

    }

}
