package com.td.product.phuotpro.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.Exp;
import com.td.product.phuotpro.R;

import java.util.List;

/**
 * Created by Admin on 5/11/2017.
 */

public class ExpAdapter extends RecyclerView.Adapter<ExpAdapter.ViewHolder>{
    private List<Exp> expList;
    Context context;
    private static ClickListener clickListener;

    public ExpAdapter(List<Exp> expList,Context context) {
        this.expList=expList;
        this.context=context;
    }

    @Override
    public ExpAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.hot_news_item,parent,false));
    }

    @Override
    public void onBindViewHolder(ExpAdapter.ViewHolder holder, int position) {
        Exp exp=expList.get(position);
        if(exp.title.length()<43){
            holder.txttitle.setText(exp.title);
        }
        else {
            holder.txttitle.setText(exp.title.substring(0,42)+" ...");
        }

        Picasso.with(context).load(exp.imagelink).into(holder.imgv);
    }

    @Override
    public int getItemCount() {
        return expList.size();
    }
    public static  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txttitle;
        ImageView imgv;
        public ViewHolder(View itemView) {
            super(itemView);
            txttitle = (TextView) itemView.findViewById(R.id.txttitle);
            imgv=(ImageView)itemView.findViewById(R.id.imgv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        ExpAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

    }
}
