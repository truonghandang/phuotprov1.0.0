package com.td.product.phuotpro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.td.product.phuotpro.Comment;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.adapter.CommentAdapter;

import java.util.ArrayList;
import java.util.List;

public class ListReviewNhahangActivity extends AppCompatActivity {
    RecyclerView recyclerreview;
    CommentAdapter commentAdapter;
    List<Comment> commentList;
    DatabaseReference databaseReference;
    String hotelid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_review_nhahang);
        Intent i = getIntent();
        hotelid = i.getStringExtra("hotelid");
        Log.d("hotelid", hotelid);
        recyclerreview = (RecyclerView) findViewById(R.id.recyclerreview);
        recyclerreview.setHasFixedSize(true);
        recyclerreview.setLayoutManager(new LinearLayoutManager(this));
        commentList = new ArrayList<>();
        commentAdapter = new CommentAdapter(commentList, getApplication());
        recyclerreview.setAdapter(commentAdapter);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("ODau").child(hotelid).child("listcomment").orderByChild("timespan").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                commentList.clear();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Comment comment = child.getValue(Comment.class);
                    commentList.add(comment);
                    commentAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
