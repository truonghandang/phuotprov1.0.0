package com.td.product.phuotpro.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.activity.LoginActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {
    Button btnsignout;
    TextView txtname, txtprovider;
    ImageView imgv;
    Context context;

    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this com.td.product.phuotpro.fragment
        View v = inflater.inflate(R.layout.fragment_about, container, false);
        btnsignout = (Button) v.findViewById(R.id.btnsignout);
        txtname = (TextView) v.findViewById(R.id.txtname);
        imgv = (ImageView) v.findViewById(R.id.imgv);
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {

                txtname.setText(user.getDisplayName());
                Picasso.with(context).load(user.getPhotoUrl()).into(imgv);
        }
        btnsignout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    LoginManager.getInstance().logOut();
                    FirebaseAuth.getInstance().signOut();
                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
            }
        });
        return v;

    }
}
