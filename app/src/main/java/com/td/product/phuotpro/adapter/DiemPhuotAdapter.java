package com.td.product.phuotpro.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.DiemPhuot;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.activity.PlaceActivity;
import java.util.List;

/**
 * Created by Admin on 6/8/2017.
 */

public class DiemPhuotAdapter extends RecyclerView.Adapter<DiemPhuotAdapter.ViewHolder>{
  private List<DiemPhuot> diemPhuotList;
  PlaceActivity context;
  private static ClickListener clickListener;
  int []background={R.color.color1,R.color.color2,R.color.color3,R.color.color4,R.color.color5,R.color.color6,R.color.color7,
      R.color.color8,R.color.color9,R.color.color10,R.color.color11,R.color.color12,R.color.color13,R.color.color14};
  int []bg={R.drawable.bg1,R.drawable.bg2,R.drawable.bg3,R.drawable.bg4,R.drawable.bg5,R.drawable.bg6,R.drawable.bg7,
      R.drawable.bg8,R.drawable.bg9,R.drawable.bg10,R.drawable.bg11,R.drawable.bg12,R.drawable.bg13,R.drawable.bg14};
  public DiemPhuotAdapter(List<DiemPhuot> diemPhuotList,PlaceActivity context) {
    this.diemPhuotList=diemPhuotList;
    this.context=context;
  }

  @Override
  public DiemPhuotAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return new ViewHolder(
        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_place,parent,false));
  }

  @Override
  public void onBindViewHolder(DiemPhuotAdapter.ViewHolder holder, int position) {
    DiemPhuot diemphuot=diemPhuotList.get(position);
    Picasso.with(context).load(diemphuot.imagelink).into(holder.imgv);
    holder.txttitle.setText(diemphuot.title);
//    holder.txtlike.setText(String.valueOf(diemphuot.like) +" Thích");
//    holder.txtview.setText(String.valueOf(diemphuot.view)+ " Xem");
////    int i=new Random().nextInt(background.length);
////    int random=background[i];
//    holder.imgv2.setBackgroundResource(background[position]);
//    holder.txtview.setBackgroundResource(bg[position]);
  }

  @Override
  public int getItemCount() {
    return diemPhuotList.size();
  }
  public static  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView txttitle,txtlike,txtview;
    ImageView imgv,imgv2;
    public ViewHolder(View itemView) {
      super(itemView);
      txttitle = (TextView) itemView.findViewById(R.id.txttitle);
//      txtlike=(TextView)itemView.findViewById(R.id.txtlike);
//      txtview=(TextView)itemView.findViewById(R.id.txtview);
      imgv=(ImageView)itemView.findViewById(R.id.imgv);
//      imgv2=(ImageView) itemView.findViewById(R.id.imgv2);
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      clickListener.onItemClick(getAdapterPosition(), v);
    }
  }
  public void setOnItemClickListener(ClickListener clickListener) {
    DiemPhuotAdapter.clickListener = clickListener;
  }

  public interface ClickListener {
    void onItemClick(int position, View v);

  }
}
