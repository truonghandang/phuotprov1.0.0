package com.td.product.phuotpro.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.activity.HotelDetailActivity;

import java.util.ArrayList;

/**
 * Created by Admin on 4/16/2017.
 */

public class HotelViewPager extends PagerAdapter {

    private ArrayList<String> listimage;
    LayoutInflater inflater;
    Context context;
    public HotelViewPager(HotelDetailActivity hotelDetailActivity, ArrayList<String> listimage){
        this.context=hotelDetailActivity;
        this.listimage=listimage;
        inflater = LayoutInflater.from(context);
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imgv;
        View v=inflater.inflate(R.layout.pager_item,container,false);
        assert v != null;
        imgv=(ImageView)v.findViewById(R.id.imgv);
        Picasso.with(context).load(listimage.get(position)).into(imgv);
        container.addView(v, 0);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return listimage.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
