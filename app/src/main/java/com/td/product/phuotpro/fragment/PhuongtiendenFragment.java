package com.td.product.phuotpro.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.activity.TransportDetailActivity;
import com.td.product.phuotpro.phuongtien.Transport;
import com.td.product.phuotpro.phuongtien.TransportAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhuongtiendenFragment extends Fragment {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference;
    ProgressDialog progressDialog;
    List<Transport> transportList;
    RecyclerView recyclerphuongtien;
    TransportAdapter transportAdapter;
    TextView textView;
    View v;
    public PhuongtiendenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_phuongtienden, container, false);
        init();
        transportList=new ArrayList<>();
        transportAdapter=new TransportAdapter(transportList,getActivity());
        recyclerphuongtien.setAdapter(transportAdapter);
        progressDialog= new ProgressDialog(getActivity());
        progressDialog.setMessage("Đang tải chờ xíu ...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        loadData();
        transportAdapter.setOnItemClickListener(new TransportAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Transport transport=transportList.get(position);
                Intent intent=new Intent(getActivity(), TransportDetailActivity.class);
                intent.putExtra("description",transport.description);
                startActivity(intent);
            }
        });
        return  v;
    }

    private void loadData() {
        firebaseDatabase=FirebaseDatabase.getInstance();
        reference=firebaseDatabase.getReference().child("PhuongTienDen").child("BinhDinh");
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Transport transport=dataSnapshot.getValue(Transport.class);
                transportList.add(transport);
                transportAdapter.notifyDataSetChanged();
                Log.d("transport", String.valueOf(transportList));
                progressDialog.dismiss();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void init() {
        textView=(TextView)v.findViewById(R.id.txtmieuta);
        recyclerphuongtien=(RecyclerView)v.findViewById(R.id.recyclerphuongtien);
        recyclerphuongtien.setHasFixedSize(true);
        recyclerphuongtien.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

}
