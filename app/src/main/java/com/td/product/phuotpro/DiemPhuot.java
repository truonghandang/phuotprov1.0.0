package com.td.product.phuotpro;

/**
 * Created by Admin on 6/8/2017.
 */

public class DiemPhuot {
  public String title;
  public String placeid;
  public String category;
  public int view;
  public String imagelink;
  public int like;
  public DiemPhuot(){

  }
  public DiemPhuot(String title, String placeid, String category, int view,
      String imagelink, int like) {
    this.title = title;
    this.placeid = placeid;
    this.category = category;
    this.view = view;
    this.imagelink = imagelink;
    this.like = like;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getPlaceid() {
    return placeid;
  }

  public void setPlaceid(String placeid) {
    this.placeid = placeid;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public int getView() {
    return view;
  }

  public void setView(int view) {
    this.view = view;
  }

  public String getImagelink() {
    return imagelink;
  }

  public void setImagelink(String imagelink) {
    this.imagelink = imagelink;
  }

  public int getLike() {
    return like;
  }

  public void setLike(int like) {
    this.like = like;
  }
}
