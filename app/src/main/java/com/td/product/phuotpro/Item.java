package com.td.product.phuotpro;

import java.util.ArrayList;

/**
 * Created by Admin on 5/13/2017.
 */

public class Item {
    public String title;
    public String cost;
    public String info;
    public String imagelink;
    public String category;
    public ArrayList<String>listimage;

    public Item(){

    }
    public Item(String title, String cost, String info, String imagelink, String category ,ArrayList<String> listimage) {
        this.title = title;
        this.cost = cost;
        this.info = info;
        this.imagelink = imagelink;
        this.category=category;
        this.listimage = listimage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getImagelink() {
        return imagelink;
    }

    public void setImagelink(String imagelink) {
        this.imagelink = imagelink;
    }

    public ArrayList<String> getListimage() {
        return listimage;
    }

    public void setListimage(ArrayList<String> listimage) {
        this.listimage = listimage;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
