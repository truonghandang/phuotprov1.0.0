package com.td.product.phuotpro;

/**
 * Created by Admin on 4/18/2017.
 */

public class Comment {
    public String name,imagelink,review,date;
    public float rating;
    public long timespan;
    public Comment(){

    }
    public Comment(String name,String imagelink,String review,float rating,String date,long timespan){
        this.name=name;
        this.imagelink=imagelink;
        this.review=review;
        this.rating=rating;
        this.date=date;
        this.timespan=timespan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagelink() {
        return imagelink;
    }

    public void setImagelink(String imagelink) {
        this.imagelink = imagelink;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
