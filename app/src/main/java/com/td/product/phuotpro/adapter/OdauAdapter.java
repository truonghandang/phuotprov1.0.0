package com.td.product.phuotpro.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.NhaHang;
import com.td.product.phuotpro.R;

import java.util.List;

/**
 * Created by Admin on 4/27/2017.
 */

public class OdauAdapter extends RecyclerView.Adapter<OdauAdapter.ViewHolder>{
    private List<NhaHang> nhaHangList;
    Context context;
    private static OdauAdapter.ClickListener clickListener;
    public OdauAdapter(Context context, List<NhaHang> nhaHangList){
        this.context=context;
        this.nhaHangList=nhaHangList;
    }

    @Override
    public OdauAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.odau_item,parent,false));
    }

    @Override
    public void onBindViewHolder(OdauAdapter.ViewHolder holder, int position) {
        NhaHang nhaHang=nhaHangList.get(position);
        holder.txttitle.setText(nhaHang.title);
        if(nhaHang.address.length()<18){
            holder.txtaddress.setText(nhaHang.address);
        }
        else {
            holder.txtaddress.setText(nhaHang.address.substring(0,14)+" ...");
        }

        Picasso.with(context).load(nhaHang.imagelink).into(holder.imgv);

    }

    @Override
    public int getItemCount() {
        return nhaHangList.size();
    }

    public static  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txttitle,txtaddress;
        ImageView imgv;
        public ViewHolder(View itemView) {
            super(itemView);
            txttitle = (TextView) itemView.findViewById(R.id.txttitle);
            txtaddress=(TextView)itemView.findViewById(R.id.txtaddress);
            imgv=(ImageView)itemView.findViewById(R.id.imgv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        OdauAdapter.clickListener=clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

    }
}
