package com.td.product.phuotpro;

/**
 * Created by Admin on 5/5/2017.
 */

public class Weather {
    public String icon,id;
    public double tempmax,tempmin;

    public Weather(){

    }
    public Weather (String icon,String id,double tempmax,double tempmin){
        this.icon=icon;
        this.id=id;
        this.tempmax=tempmax;
        this.tempmin=tempmin;
    }
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getTempmax() {
        return tempmax;
    }

    public void setTempmax(double tempmax) {
        this.tempmax = tempmax;
    }

    public double getTempmin() {
        return tempmin;
    }

    public void setTempmin(double tempmin) {
        this.tempmin = tempmin;
    }
}
