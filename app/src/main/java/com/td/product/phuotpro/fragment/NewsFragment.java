package com.td.product.phuotpro.fragment;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.td.product.phuotpro.News;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.XMLDOMParser;
import com.td.product.phuotpro.activity.NewsActivity;
import com.td.product.phuotpro.adapter.NewsAdapter2;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {
    View v;
    List<News> newsList;
    RecyclerView recyclernews;
    NewsAdapter2 newsAdapter;
    String linkrss = "http://vnexpress.net/rss/du-lich.rss";

    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this com.td.product.phuotpro.fragment
        v = inflater.inflate(R.layout.fragment_news, container, false);
        init();
        newsList = new ArrayList<News>();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new ReadXML().execute(linkrss);

            }
        });
        return v;
    }

    private void init() {
        recyclernews = (RecyclerView) v.findViewById(R.id.recyclernews);
        recyclernews.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclernews.setLayoutManager(layoutManager);
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(),layoutManager.getOrientation());
//        recyclernews.addItemDecoration(dividerItemDecoration);
    }

    public class ReadXML extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            return docNoiDung_Tu_URL(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (TextUtils.isEmpty(s)) {
                return;
            }
            XMLDOMParser parser = new XMLDOMParser();
            Document document = parser.getDocument(s);
            NodeList nodeList = document.getElementsByTagName("item");
            NodeList nodelistdes = document.getElementsByTagName("description");
            for (int i = 0; i < nodeList.getLength(); i++) {
                News news = new News();
                String cdata = nodelistdes.item(i + 1).getTextContent();
                Pattern pattern = Pattern.compile("<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>");
                Matcher matcher = pattern.matcher(cdata);

                if (matcher.find()) {
                    news.setImagelink(matcher.group(1));
                }
                String des;
                int start = cdata.indexOf("</br>");
                int start2 = cdata.indexOf("r>");
                if (start >= 0) {
                    des = cdata.substring(start + 5);
                    Log.d("chuoi cắt", des);
                    news.setDecription(des);
                } else {
                    des = cdata.substring(start2 + 5);
                    Log.d("chuoi cắt", des);
                    news.setDecription(des);
                }
//                news.setDecription(cdata.substring(start,end));
                Element element = (Element) nodeList.item(i);
                news.setTitle(parser.getValue(element, "title"));
                news.setLink(parser.getValue(element, "link"));
                if (des.equals("escription") == false) {
                    newsList.add(news);
                }
            }
            newsAdapter = new NewsAdapter2(newsList, getActivity());
            recyclernews.setAdapter(newsAdapter);
            newsAdapter.notifyDataSetChanged();
            newsAdapter.setOnItemClickListener(new NewsAdapter2.ClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    News news = newsList.get(position);
                    Intent i = new Intent(getActivity(), NewsActivity.class);
                    i.putExtra("link", news.getLink());
                    startActivity(i);
                }
            });
        }
    }

    private static String docNoiDung_Tu_URL(String theUrl) {
        StringBuilder content = new StringBuilder();

        try {
            // create a url object
            URL url = new URL(theUrl);

            // create a urlconnection object
            URLConnection urlConnection = url.openConnection();

            // wrap the urlconnection in a bufferedreader
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String line;

            // read from the urlconnection via the bufferedreader
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line + "\n");
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content.toString();
    }
}
