package com.td.product.phuotpro.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.Item;
import com.td.product.phuotpro.R;

import java.util.List;

/**
 * Created by Admin on 5/13/2017.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder>{


    private List<Item> itemList;
    Context context;
    private static ClickListener clickListener;

    public ItemAdapter(List<Item> itemList, Context context) {
        this.itemList=itemList;
        this.context=context;
    }

    @Override
    public ItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.hot_news_item,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemAdapter.ViewHolder holder, int position) {
        Item item=itemList.get(position);
        if(item.title.length()<43){
            holder.txttitle.setText(item.title);
        }
        else {
            holder.txttitle.setText(item.title.substring(0,42)+" ...");
        }

        Picasso.with(context).load(item.imagelink).into(holder.imgv);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
    public static  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txttitle;
        ImageView imgv;
        public ViewHolder(View itemView) {
            super(itemView);
            txttitle = (TextView) itemView.findViewById(R.id.txttitle);
            imgv=(ImageView)itemView.findViewById(R.id.imgv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        ItemAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

    }
}
