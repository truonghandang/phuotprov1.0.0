package com.td.product.phuotpro.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.DiemDen.Place;
import com.td.product.phuotpro.R;

import java.util.List;

/**
 * Created by Admin on 4/16/2017.
 */

public class HotelAdapter extends RecyclerView.Adapter<HotelAdapter.ViewHolder>{


    private List<Place> listPlace;
    Context context;
    private static ClickListener clickListener;

    public HotelAdapter(List<Place> listPlace,Context context) {
        this.listPlace=listPlace;
        this.context=context;
    }

    @Override
    public HotelAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.hotel_item,parent,false));
    }

    @Override
    public void onBindViewHolder(HotelAdapter.ViewHolder holder, int position) {
        Place place=listPlace.get(position);
        holder.txttitle.setText(place.title);
        Picasso.with(context).load(place.imagelink).into(holder.imgv);
        holder.ratingBar.setRating(place.rank);
        holder.txtaddress.setText(place.address);
        holder.txtcost.setText(place.cost);
//        GPSTracker gpsTracker=new GPSTracker(context);
//        if(gpsTracker.canGetLocation()){
//            double lat1=gpsTracker.getLatitude();
//            double lon1=gpsTracker.getLatitude();
//            double lat2=place.latitude;
//            double lon2=place.longtitude;
//            double theta = lon1 - lon2;
//            double dist = Math.sin(lat2 * Math.PI / 180.0)
//                    * Math.sin(lat2 * Math.PI / 180.0)
//                    + Math.cos(lat1 * Math.PI / 180.0)
//                    * Math.cos(lat2 * Math.PI / 180.0)
//                    * Math.cos(theta * Math.PI / 180.0);
//            dist = Math.acos(dist);
//            dist = dist * Math.PI / 180.0;
//            dist = dist * 60 * 1.1515;
//            holder.txtdistance.setText((int) dist);
//        }




    }

    @Override
    public int getItemCount() {
        return listPlace.size();
    }
    public static  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txttitle,txtaddress,txtcost;
        ImageView imgv;
        RatingBar ratingBar;
        public ViewHolder(View itemView) {
            super(itemView);
            txttitle = (TextView) itemView.findViewById(R.id.txttitle);
//            txtdistance = (TextView) itemView.findViewById(R.id.txtdistance);
            imgv=(ImageView)itemView.findViewById(R.id.imgv);
            ratingBar=(RatingBar)itemView.findViewById(R.id.ratingBar);
            txtcost=(TextView)itemView.findViewById(R.id.txtcost);
//            txtphone=(TextView)itemView.findViewById(R.id.txtphone);
            txtaddress=(TextView)itemView.findViewById(R.id.txtaddress) ;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        HotelAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

    }
}
