package com.td.product.phuotpro.DiemDen;

/**
 * Created by Admin on 4/7/2017.
 */

public class MenuPlace {
    public String title;
    public Integer img;
    public  MenuPlace(){

    }
    public MenuPlace(String title,Integer img){
        this.title=title;
        this.img=img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getImg() {
        return img;
    }

    public void setImg(Integer img) {
        this.img = img;
    }
}
