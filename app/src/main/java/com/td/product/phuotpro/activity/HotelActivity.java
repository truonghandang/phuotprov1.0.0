package com.td.product.phuotpro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.td.product.phuotpro.DiemDen.Place;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.adapter.HotelAdapter;

import java.util.ArrayList;
import java.util.List;

public class HotelActivity extends AppCompatActivity {
    RecyclerView recyclerhotel;
    List<Place>placeList;
    HotelAdapter hotelAdapter;
    DatabaseReference reference;
    ArrayList<String> listlink;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel);
        init();
        placeList=new ArrayList<>();
        hotelAdapter=new HotelAdapter(placeList,getApplication());
        recyclerhotel.setAdapter(hotelAdapter);
        reference=FirebaseDatabase.getInstance().getReference();
        loadData();
        hotelAdapter.setOnItemClickListener(new HotelAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Place place=placeList.get(position);
                listlink=new ArrayList<String>();
                for(int i=0;i<place.listimage.size();i++){
                    listlink.add(place.listimage.get(i));
                }
                Intent intent=new Intent(getApplication(),HotelDetailActivity.class);
                intent.putStringArrayListExtra("listlink", (ArrayList<String>) listlink);
                intent.putExtra("info",place.info);
                intent.putExtra("address",place.address);
                intent.putExtra("phone",place.phone);
                intent.putExtra("rank",place.rank);
                intent.putExtra("cost",place.cost);
                intent.putExtra("title",place.title);
                intent.putExtra("hotelid",place.hotelid);
                intent.putExtra("lat",place.latitude);
                intent.putExtra("lon",place.longtitude);
                startActivity(intent);

            }
        });

    }

    private void loadData() {
        reference.child("Hotel").orderByChild("placeid").equalTo("BinhDinh").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                placeList.clear();
                for(DataSnapshot child:dataSnapshot.getChildren()){
                    Place place=child.getValue(Place.class);
                    placeList.add(place);
                    hotelAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void init() {
        recyclerhotel=(RecyclerView)findViewById(R.id.recyclerhotel);
        recyclerhotel.setHasFixedSize(true);
        recyclerhotel.setLayoutManager(new LinearLayoutManager(getApplication()));
    }
}
