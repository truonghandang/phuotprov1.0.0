package com.td.product.phuotpro.DiemDen;

import java.util.ArrayList;

/**
 * Created by Admin on 4/2/2017.
 */

public class Place {

    public String title;
    public String imagelink;
    public String description;
    public String placeid;
    public String info;
    public String note;
    public String route;
    public double latitude;
    public double longtitude;
    public String address;
    public String phone;
    public long rank;
    public String cost;
    public String hotelid;
    public String time;
//    public List<Transport> phuongtienden;
    public ArrayList<String> listimage;
    public  Place(){

    }
    public Place(String title, String imagelink, String description, String placeid, ArrayList<String>listimage,String info,String note,String route,double lattitude,double longtitude,String address,long rank,String phone,String cost,String hotelid,String time){
        this.title=title;
        this.imagelink=imagelink;
        this.description=description;
        this.placeid=placeid;
        this.info=info;
        this.listimage=listimage;
        this.note=note;
        this.route=route;
        this.latitude=lattitude;
        this.longtitude=longtitude;
        this.address=address;
        this.rank=rank;
        this.phone=phone;
        this.cost=cost;
        this.hotelid=hotelid;
        this.time=time;

//        this.phuongtienden=phuongtienden;
    }


    public String getTitle() {
        return title;
    }

    public String getPlaceid() {
        return placeid;
    }

    public void setPlaceid(String placeid) {
        this.placeid = placeid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImagelink() {
        return imagelink;
    }

    public void setImagelink(String imagelink) {
        this.imagelink = imagelink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public ArrayList<String> getListimage() {
        return listimage;
    }

    public void setListimage(ArrayList<String> listimage) {
        this.listimage = listimage;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }
    public long getRank() {
        return rank;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getHotelid() {
        return hotelid;
    }

    public void setHotelid(String hotelid) {
        this.hotelid = hotelid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
