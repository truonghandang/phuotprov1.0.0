package com.td.product.phuotpro.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.DiaDiemNho;
import com.td.product.phuotpro.R;

import java.util.List;

/**
 * Created by Admin on 4/24/2017.
 */

public class PlayAdapter extends RecyclerView.Adapter<PlayAdapter.ViewHolder>{

    private List<DiaDiemNho> diaDiemNhoList;
    Context context;
    private static ClickListener clickListener;

    public PlayAdapter(List<DiaDiemNho> diaDiemNhoList,Context context) {
        this.diaDiemNhoList=diaDiemNhoList;
        this.context=context;
    }

    @Override
    public PlayAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.play_item,parent,false));
    }

    @Override
    public void onBindViewHolder(PlayAdapter.ViewHolder holder, int position) {
        DiaDiemNho diaDiemNho=diaDiemNhoList.get(position);
        holder.txttitle.setText(diaDiemNho.title);
        Picasso.with(context).load(diaDiemNho.imagelink).into(holder.imgv);
    }

    @Override
    public int getItemCount() {
        return diaDiemNhoList.size();
    }
    public static  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txttitle;
        ImageView imgv;
        public ViewHolder(View itemView) {
            super(itemView);
            txttitle = (TextView) itemView.findViewById(R.id.txttitle);
//            txtdistance = (TextView) itemView.findViewById(R.id.txtdistance);
            imgv=(ImageView)itemView.findViewById(R.id.imgv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        PlayAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

    }
}