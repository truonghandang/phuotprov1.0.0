package com.td.product.phuotpro.phuongtien;

/**
 * Created by Admin on 4/12/2017.
 */

public class Transport {
    public String title,linkimage,description;
    public Transport(){

    }
    public Transport(String title,String linkimage,String description){
        this.title=title;
        this.linkimage=linkimage;
        this.description=description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLinkimage() {
        return linkimage;
    }

    public void setLinkimage(String linkimage) {
        this.linkimage = linkimage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
