package com.td.product.phuotpro.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.News;
import com.td.product.phuotpro.R;

import java.util.List;

/**
 * Created by Admin on 5/24/2017.
 */

public class NewsAdapter2 extends RecyclerView.Adapter<NewsAdapter2.ViewHolder>{

    private List<News> newsList;
    Context context;
    private static ClickListener clickListener;

    public NewsAdapter2(List<News> newsList,Context context) {
        this.newsList=newsList;
        this.context=context;
    }

    @Override
    public NewsAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.news_item,parent,false));
    }

    @Override
    public void onBindViewHolder(NewsAdapter2.ViewHolder holder, int position) {
        News news=newsList.get(position);
        holder.txttitle.setText(news.getTitle());
        if(news.getDecription().length()<50){
            holder.txtdes.setText(news.getDecription());
        }
        else {
            holder.txtdes.setText(news.getDecription().substring(0,50)+" ...");
        }
        Picasso.with(context).load(news.getImagelink()).into(holder.imgv);
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }
    public static  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txttitle,txtdes;
        ImageView imgv;
        public ViewHolder(View itemView) {
            super(itemView);
            txttitle = (TextView) itemView.findViewById(R.id.txttitle);
            txtdes = (TextView) itemView.findViewById(R.id.txtdes);
            imgv=(ImageView)itemView.findViewById(R.id.imgv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        NewsAdapter2.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

    }
}