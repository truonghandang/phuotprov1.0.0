package com.td.product.phuotpro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.td.product.phuotpro.Comment;
import com.td.product.phuotpro.R;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReviewHotelActivity extends AppCompatActivity {
    TextView txtreview;
    RatingBar ratingBar;
    EditText edttitle,edtreview;
    Button btnsubmit;
    float rating;
    long timestamp;
    String hotelid,name,imagelink,review,date;
    FirebaseUser user;
    DatabaseReference database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_hotel);
        init();
        final Intent i=getIntent();
        rating=i.getFloatExtra("reviewhotel",0);
        hotelid=i.getStringExtra("hotelid");
        ratingBar.setRating(rating);
        user= FirebaseAuth.getInstance().getCurrentUser();
        database=FirebaseDatabase.getInstance().getReference();
        if(rating==1.0){
            txtreview.setText("Ghét");
        }
        if(rating==2.0){
            txtreview.setText("Không Thích");
        }
        if(rating==3.0){
            txtreview.setText("Cũng Được");
        }
        if(rating==4.0){
            txtreview.setText("Thích");
        }
        if(rating==5.0){
            txtreview.setText("Rất Thích");
        }
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if(rating==1){
                    txtreview.setText("Ghét");
                }
                if(rating==2){
                    txtreview.setText("Không Thích");
                }
                if(rating==3){
                    txtreview.setText("Cũng Được");
                }
                if(rating==4){
                    txtreview.setText("Thích");
                }
                if(rating==5){
                    txtreview.setText("Rất Thích");
                }
            }
        });
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                review=edtreview.getText().toString();
                if(review.isEmpty()||review.length()<3){
                    edtreview.setError("Độ dài đánh giá tối thiểu 3 ký tụ");
                }
                else {
                    name=user.getDisplayName();
                    imagelink= String.valueOf(user.getPhotoUrl());
                    Timestamp time = new Timestamp(System.currentTimeMillis());
                    timestamp=-1*(time.getTime());
                    SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
                    Date todayDate = new Date();
                    date=currentDate.format(todayDate);
                    Comment comment=new Comment(name,imagelink,review,ratingBar.getRating(),date,timestamp);
                    database.child("Hotel").child(hotelid).child("listcomment").push().setValue(comment);
                    finish();

                }
            }
        });
    }

    private void init() {
        txtreview=(TextView)findViewById(R.id.txtreview);
        ratingBar=(RatingBar)findViewById(R.id.ratingBar);
        edtreview=(EditText)findViewById(R.id.edtreview);
//        edttitle=(EditText)findViewById(R.id.edttitle);
        btnsubmit=(Button)findViewById(R.id.btnsubmit);
    }
}
