package com.td.product.phuotpro.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.DiemDen.Place;
import com.td.product.phuotpro.R;

import java.util.List;

/**
 * Created by Admin on 4/26/2017.
 */

public class AnGiAdapter extends RecyclerView.Adapter<AnGiAdapter.ViewHolder>{
    private List<Place> placeList;
    Context context;
    private static AnGiAdapter.ClickListener clickListener;
    public AnGiAdapter(Context context,List<Place> placeList){
        this.context=context;
        this.placeList=placeList;
    }

    @Override
    public AnGiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.angi_item,parent,false));
    }

    @Override
    public void onBindViewHolder(AnGiAdapter.ViewHolder holder, int position) {
        Place place=placeList.get(position);
        holder.txttitle.setText(place.title);
        holder.txtdes.setText(place.info.substring(0,100));
        Picasso.with(context).load(place.imagelink).into(holder.imgv);

    }

    @Override
    public int getItemCount() {
        return placeList.size();
    }

    public static  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txttitle,txtdes;
        ImageView imgv;
        public ViewHolder(View itemView) {
            super(itemView);
            txttitle = (TextView) itemView.findViewById(R.id.txttitle);
            txtdes=(TextView)itemView.findViewById(R.id.txtdes);
            imgv=(ImageView)itemView.findViewById(R.id.imgv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
    public void setOnItemClickListener(ClickListener clickListener) {
       AnGiAdapter.clickListener=clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

    }
}
