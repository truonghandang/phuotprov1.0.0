package com.td.product.phuotpro.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.Weather;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Admin on 5/5/2017.
 */

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder>{
    private List<Weather> weatherList;
    Context context;
    int day1,day2,day3,day4,day5,day6,day7;
    public WeatherAdapter(Context context, List<Weather> weatherList){
        this.context=context;
        this.weatherList=weatherList;
    }

    @Override
    public WeatherAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_item,parent,false));
    }

    @Override
    public void onBindViewHolder(WeatherAdapter.ViewHolder holder, int position) {
        Weather weather=weatherList.get(position);
        Log.d("tempmax",String.valueOf(Math.round(weather.getTempmax())));
        holder.txttempmax.setText(String.valueOf(Math.round(weather.getTempmax()))+" °");
        holder.txttempmin.setText(String.valueOf(Math.round(weather.getTempmin()))+" °");
        String id=weather.getId();
        if(id=="900"){
            holder.iconweather.setBackgroundResource(R.drawable.wtornado);
        }
        if(id=="901" || id=="902"){
            holder.iconweather.setBackgroundResource(R.drawable.wtropical_storm);
        }
        if(id=="903"){
            holder.iconweather.setBackgroundResource(R.drawable.wcold);
        }
        if(id=="904"){
            holder.iconweather.setBackgroundResource(R.drawable.whot);
        }
        if(id=="905"){
            holder.iconweather.setBackgroundResource(R.drawable.wwind);
        }
        if(id=="906"){
            holder.iconweather.setBackgroundResource(R.drawable.whail);
        }
        if(id=="951"){
            Calendar calendar=Calendar.getInstance();
            int currenthour=calendar.get(Calendar.HOUR);
            if(currenthour>=6 && currenthour<=18){
                holder.iconweather.setBackgroundResource(R.drawable.w1d);
            }
            else {
                holder.iconweather.setBackgroundResource(R.drawable.w1n);
            }
        }
        if(id=="952" || id=="953"){
            holder.iconweather.setBackgroundResource(R.drawable.wwind);
        }
        if(id=="954" || id=="955"){
            holder.iconweather.setBackgroundResource(R.drawable.wwind);
        }
        if(id=="956" || id=="957"){
            holder.iconweather.setBackgroundResource(R.drawable.wwind);
        }
        if(id=="958"){
            holder.iconweather.setBackgroundResource(R.drawable.wwind);
        }
        if(id=="959" || id=="960"){
            holder.iconweather.setBackgroundResource(R.drawable.wtropical_storm);
        }
        if(id=="961" || id=="962"){
            holder.iconweather.setBackgroundResource(R.drawable.wtropical_storm);
        }
        else {
            switch(weather.getIcon()){
                case "01d":
                    holder.iconweather.setBackgroundResource(R.drawable.w1d);
                    break;
                case "01n":
                    holder.iconweather.setBackgroundResource(R.drawable.w1n);
                    break;
                case "02d":
                    holder.iconweather.setBackgroundResource(R.drawable.w2d);
                    break;
                case "02n":
                    holder.iconweather.setBackgroundResource(R.drawable.w2n);
                    break;
                case "03d":
                    holder.iconweather.setBackgroundResource(R.drawable.w3d);
                    break;
                case "03n":
                    holder.iconweather.setBackgroundResource(R.drawable.w3n);
                    break;
                case "04d":
                    holder.iconweather.setBackgroundResource(R.drawable.w4d);
                    break;
                case "04n":
                    holder.iconweather.setBackgroundResource(R.drawable.w4n);
                    break;
                case "09d":
                    holder.iconweather.setBackgroundResource(R.drawable.w9d);
                    break;
                case "09n":
                    holder.iconweather.setBackgroundResource(R.drawable.w9n);
                    break;
                case "10d":
                    holder.iconweather.setBackgroundResource(R.drawable.w10d);
                    break;
                case "10n":
                    holder.iconweather.setBackgroundResource(R.drawable.w10n);
                    break;
                case "11d":
                    holder.iconweather.setBackgroundResource(R.drawable.w11d);
                    break;
                case "11n":
                    holder.iconweather.setBackgroundResource(R.drawable.w11n);
                    break;
                case "13d":
                    holder.iconweather.setBackgroundResource(R.drawable.w13d);
                    break;
                case "13n":
                    holder.iconweather.setBackgroundResource(R.drawable.w13n);
                    break;
                case "50d":
                    holder.iconweather.setBackgroundResource(R.drawable.w10d);
                    break;
                case "50n":
                    holder.iconweather.setBackgroundResource(R.drawable.w10n);
                    break;
                default:
                    Picasso.with(context).load("http://openweathermap.org/img/w/"+weather.getIcon()+".png").into(holder.iconweather);
                    break;
            }
        }

//        Picasso.with(context).load(weather.getIcon()).into(holder.iconwweather);
        Calendar cal = Calendar.getInstance();
        day1 = cal.get(Calendar.DAY_OF_WEEK);
        Log.d("day", String.valueOf(day1));
        day2=day1+1;
        if(day2>7){
            day2=1;
        }
        day3=day2+1;
        if(day3>7){
            day3=1;
        }
        day4=day3+1;
        if(day4>7){
            day4=1;
        }
        day5=day4+1;
        if(day5>7){
            day5=1;
        }
        day6=day5+1;
        if(day6>7){
            day6=1;
        }
        day7=day6+1;
        if(day7>7){
            day7=1;
        }
        ArrayList<Integer>listday=new ArrayList<Integer>();
        listday.add(day1);listday.add(day2);listday.add(day3);listday.add(day4);listday.add(day5);listday.add(day6);listday.add(day7);
       if(listday.get(position)==1){
            holder.txtthu.setText("Chủ Nhật");
       }
       if(listday.get(position)==2){
           holder.txtthu.setText("Thứ Hai");
        }
        if(listday.get(position)==3){
           holder.txtthu.setText("Thứ Ba");
        }
        if(listday.get(position)==4){
           holder.txtthu.setText("Thứ Tư");
        }
        if(listday.get(position)==5){
            holder.txtthu.setText("Thứ Năm");
        }
        if(listday.get(position)==6){
            holder.txtthu.setText("Thứ Sáu");
        }
        if(listday.get(position)==7){
            holder.txtthu.setText("Thứ Bảy");
        }
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    public static  class ViewHolder extends RecyclerView.ViewHolder{
        TextView txttempmax,txttempmin,txtthu;
        ImageView iconweather;
        public ViewHolder(View itemView) {
            super(itemView);
            txtthu=(TextView)itemView.findViewById(R.id.txtthu);
            txttempmax = (TextView) itemView.findViewById(R.id.txttempmax);
            txttempmin=(TextView)itemView.findViewById(R.id.txttempmin);
            iconweather=(ImageView)itemView.findViewById(R.id.iconweather);
        }
    }
}
