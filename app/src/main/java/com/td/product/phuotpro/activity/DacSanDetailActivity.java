package com.td.product.phuotpro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.td.product.phuotpro.R;
import com.td.product.phuotpro.adapter.PlaceViewPager;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class DacSanDetailActivity extends AppCompatActivity {
    TextView txttitle,txtdes;
    PlaceViewPager placeViewPager;
    ViewPager viewPager;
    ArrayList<String> listlink;
    String title,info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dac_san_detail);
        listlink=new ArrayList<String>();
        Intent i = getIntent();
        listlink = i.getStringArrayListExtra("listlink");
        title=i.getStringExtra("title");
        info=i.getStringExtra("info");
        viewPager=(ViewPager)findViewById(R.id.viewpager);
        placeViewPager=new PlaceViewPager(getApplication(),listlink);
        viewPager.setAdapter(placeViewPager);
        CircleIndicator indicator=(CircleIndicator)findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);
        placeViewPager.registerDataSetObserver(indicator.getDataSetObserver());
        txttitle=(TextView)findViewById(R.id.txttitle);
        txtdes=(TextView)findViewById(R.id.txtdes);
        txttitle.setText(title);
        txtdes.setText(info);

    }
}
