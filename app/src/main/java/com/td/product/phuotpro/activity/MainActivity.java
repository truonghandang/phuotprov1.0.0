package com.td.product.phuotpro.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.td.product.phuotpro.fragment.AboutFragment;
import com.td.product.phuotpro.fragment.HomeFragment;
import com.td.product.phuotpro.fragment.LoveFragment;
import com.td.product.phuotpro.fragment.MapFragment;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.td.product.phuotpro.R.layout.activity_main);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(com.td.product.phuotpro.R.id.navigation);
        disableShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(com.td.product.phuotpro.R.id.navigation_home);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case com.td.product.phuotpro.R.id.navigation_home:
                    HomeFragment homefragment = new HomeFragment();
                    getSupportFragmentManager().beginTransaction().replace(com.td.product.phuotpro.R.id.content, homefragment).commit();
                    return true;
                case com.td.product.phuotpro.R.id.navigation_dashboard:
                    MapFragment mapFragment = new MapFragment();
                    getSupportFragmentManager().beginTransaction().replace(com.td.product.phuotpro.R.id.content, mapFragment).commit();
                    return true;
                case com.td.product.phuotpro.R.id.navigation_notifications:
                    LoveFragment loveFragment = new LoveFragment();
                    getSupportFragmentManager().beginTransaction().replace(com.td.product.phuotpro.R.id.content, loveFragment).commit();
                    return true;
                case com.td.product.phuotpro.R.id.navigation_about:
                    AboutFragment aboutfragment = new AboutFragment();
                    getSupportFragmentManager().beginTransaction().replace(com.td.product.phuotpro.R.id.content, aboutfragment).commit();
                    return true;
                default:
                    HomeFragment f = new HomeFragment();
                    getSupportFragmentManager().beginTransaction().replace(com.td.product.phuotpro.R.id.content, f).commit();
                    return false;

            }
        }

    };

    public void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e(TAG, "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            Log.e(TAG, "Unable to change value of shift mode");
        }
    }
}


