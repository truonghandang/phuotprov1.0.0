package com.td.product.phuotpro.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.td.product.phuotpro.Exp;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.activity.InfoActivity;
import com.td.product.phuotpro.adapter.ExpAdapter2;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpFragment extends Fragment {
    View v;
    DatabaseReference referenceExp;
    List<Exp> expList;
    RecyclerView recyclerexp;
    ExpAdapter2 expAdapter;
    Toolbar toolbar;
    SearchView searchView;
    public ExpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this com.td.product.phuotpro.fragment
        v= inflater.inflate(R.layout.fragment_exp, container, false);
        init();
        expList=new ArrayList<>();
        expAdapter=new ExpAdapter2(expList,getActivity());
        recyclerexp.setAdapter(expAdapter);
        referenceExp= FirebaseDatabase.getInstance().getReference();
        loadData();
        expAdapter.setOnItemClickListener(new ExpAdapter2.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Exp exp=expList.get(position);
                Intent i=new Intent(getActivity(), InfoActivity.class);
                i.putExtra("info",exp.info);
                startActivity(i);
            }
        });
        return v;
    }

    private void loadData() {
        referenceExp.child("KinhNghiem").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot child:dataSnapshot.getChildren()){
                    Exp exp=child.getValue(Exp.class);
                    expList.add(exp);
                    expAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void init() {
        recyclerexp=(RecyclerView)v.findViewById(R.id.recyclerexp);
        recyclerexp.setHasFixedSize(true);
        LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        recyclerexp.setLayoutManager(layoutManager);
        searchView=(SearchView)v.findViewById(R.id.searchview);
        searchView.setOnQueryTextListener(new OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText=newText.toLowerCase();
                ArrayList<Exp> newList=new ArrayList<Exp>();
                for(Exp exp:expList){
                    String name=exp.getTitle().toLowerCase();
                    if(name.contains(newText)){
                        newList.add(exp);
                    }
                }
                expAdapter.setFilter(newList);
                return true;
            }
        });
//        toolbar=(Toolbar)v.findViewById(R.id.toolbar);
//        AppCompatActivity activity = (AppCompatActivity) getActivity();
//        activity.setSupportActionBar(toolbar);
//        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(),layoutManager.getOrientation());
//        recyclerexp.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_search,menu);
        MenuItem menuItem=menu.findItem(R.id.search);
        SearchView searchView=(SearchView)MenuItemCompat.getActionView(menuItem);
//        searchView.setOnQueryTextListener(new OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                newText=newText.toLowerCase();
//                ArrayList<Exp> newList=new ArrayList<Exp>();
//                for(Exp exp:expList){
//                    String name=exp.getTitle().toLowerCase();
//                    if(name.contains(newText)){
//                        newList.add(exp);
//                    }
//                }
//                expAdapter.setFilter(newList);
//                return true;
//            }
//        });
        super.onCreateOptionsMenu(menu, inflater);
    }
}
