package com.td.product.phuotpro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.td.product.phuotpro.Comment;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.adapter.PlaceViewPager;
import com.td.product.phuotpro.adapter.ReviewAdapter;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class PlayDetailActivity extends AppCompatActivity {
    LinearLayout viewdanhgia;
    TextView txtinfo,txttitle,txtaddress,txtreview,txtavgrating,txtviewall,txtnoreview;
    RatingBar reviewplace,review;
    ViewPager viewPager;
    PlaceViewPager adapter;
    ArrayList<String> listlink;
    String info,address,title,hotelid;
    DatabaseReference databaseReference,databasereview;
    private GoogleMap myMap;
    double lat,lon;
    List<Comment> commentList,commentList1;
    ProgressBar progressBar5,progressBar4,progressBar3,progressBar2,progressBar1;
    RecyclerView recyclerreview;
    ReviewAdapter reviewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_detail);
        init();
        listlink=new ArrayList<String>();
        Intent i = getIntent();
        listlink = i.getStringArrayListExtra("listlink");
        info=i.getStringExtra("info");
        address=i.getStringExtra("address");
        title=i.getStringExtra("title");
        hotelid=i.getStringExtra("hotelid");
        lat=i.getDoubleExtra("lat",9);
        lon=i.getDoubleExtra("lon",9);
        adapter=new PlaceViewPager(this,listlink);
        viewPager.setAdapter(adapter);
        CircleIndicator indicator=(CircleIndicator)findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());
        txtinfo.setText(info);
        txttitle.setText(title);
        txtaddress.setText(address);
        reviewplace.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Intent intent=new Intent(getApplication(),ReviewPlaceActivity.class);
                intent.putExtra("reviewplace",rating);
                intent.putExtra("hotelid",hotelid);
                startActivity(intent);


            }
        });
        SupportMapFragment mapFragment= (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                myMap = googleMap;

                // Add a marker in Sydney and move the camera
                LatLng hotel = new LatLng(lat, lon);
                myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(hotel, 13));
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(hotel)             // Sets the center of the map to location user
                        .zoom(15)                   // Sets the zoom
                        .bearing(90)                // Sets the orientation of the camera to east
                        .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                myMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                // Thêm Marker cho Map:
                MarkerOptions option = new MarkerOptions();
                option.title(title);
                option.snippet("....");
                option.position(hotel);
                Marker currentMarker = myMap.addMarker(option);
                currentMarker.showInfoWindow();
            }
        });
        commentList=new ArrayList<>();

        databasereview= FirebaseDatabase.getInstance().getReference().child("DiaDiemNho").child(hotelid).child("listcomment");
        databaseReference= FirebaseDatabase.getInstance().getReference().child("DiaDiemNho").child(hotelid).child("listcomment");
        databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.getChildrenCount()>0){
                        viewdanhgia.setVisibility(View.VISIBLE);
                        txtnoreview.setVisibility(View.GONE);
                        float avgrating = 0;
                        int countrate5 = 0, countrate4 = 0, countrate3 = 0, countrate2 = 0, countrate1 = 0;
                        commentList.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            Comment comment = child.getValue(Comment.class);
                            commentList.add(comment);
                            avgrating += comment.rating;
                            if (comment.rating == 5.0) {
                                countrate5 += 1;
                            }
                            if (comment.rating == 4.0) {
                                countrate4 += 1;
                            }
                            if (comment.rating == 3.0) {
                                countrate3 += 1;
                            }
                            if (comment.rating == 2.0) {
                                countrate2 += 1;
                            }
                            if (comment.rating == 1.0) {
                                countrate1 += 1;
                            }
                        }
                        progressBar5.setProgress(countrate5);
                        progressBar5.setMax((int) dataSnapshot.getChildrenCount());
                        progressBar4.setProgress(countrate4);
                        progressBar4.setMax((int) dataSnapshot.getChildrenCount());
                        progressBar3.setProgress(countrate3);
                        progressBar3.setMax((int) dataSnapshot.getChildrenCount());
                        progressBar2.setProgress(countrate2);
                        progressBar2.setMax((int) dataSnapshot.getChildrenCount());
                        progressBar1.setProgress(countrate1);
                        progressBar1.setMax((int) dataSnapshot.getChildrenCount());
                        float a = avgrating / dataSnapshot.getChildrenCount();
                        txtavgrating.setText(String.valueOf((double) Math.round(a * 10) / 10));
                        txtreview.setText(dataSnapshot.getChildrenCount() + " Đánh giá");
                        review.setRating(avgrating / dataSnapshot.getChildrenCount());
                    }
                    }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }

            });
        commentList1=new ArrayList<>();
        reviewAdapter=new ReviewAdapter(commentList1,getApplication());
        recyclerreview.setAdapter(reviewAdapter);
        databasereview.orderByChild("timespan").limitToFirst(2).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                    commentList1.clear();
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        Comment comment = child.getValue(Comment.class);
                        commentList1.add(comment);
                        reviewAdapter.notifyDataSetChanged();
                    }
                }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        reviewAdapter.setOnItemClickListener(new ReviewAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent i=new Intent(getApplication(),ListReviewPlaceActivity.class);
                i.putExtra("hotelid",hotelid);
                startActivity(i);
            }
        });
        txtviewall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplication(),ListReviewPlaceActivity.class);
                i.putExtra("hotelid",hotelid);
                startActivity(i);
            }
        });
    }

    private void init() {
        txtnoreview=(TextView)findViewById(R.id.txtnoreview);
        viewdanhgia=(LinearLayout)findViewById(R.id.viewdanhgia);
        recyclerreview=(RecyclerView)findViewById(R.id.recyclerreview);
        recyclerreview.setHasFixedSize(true);
        recyclerreview.setLayoutManager(new LinearLayoutManager(getApplication()));
        txttitle=(TextView)findViewById(R.id.txttitle);
        txtinfo=(TextView)findViewById(R.id.txtinfo);
        txtaddress=(TextView)findViewById(R.id.txtaddress);
        reviewplace=(RatingBar)findViewById(R.id.reviewplace);
        txtreview=(TextView)findViewById(R.id.txtreview);
        txtviewall=(TextView)findViewById(R.id.txtviewall);
        review=(RatingBar)findViewById(R.id.review);
        viewPager=(ViewPager)findViewById(R.id.viewpager);
        txtavgrating=(TextView)findViewById(R.id.txtavgrating);
        progressBar5=(ProgressBar)findViewById(R.id.progress5);
        progressBar4=(ProgressBar)findViewById(R.id.progress4);
        progressBar3=(ProgressBar)findViewById(R.id.progress3);
        progressBar2=(ProgressBar)findViewById(R.id.progress2);
        progressBar1=(ProgressBar)findViewById(R.id.progress1);
    }
}
