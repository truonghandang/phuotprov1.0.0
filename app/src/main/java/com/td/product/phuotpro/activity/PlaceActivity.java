package com.td.product.phuotpro.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.td.product.phuotpro.DiemPhuot;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.adapter.DiemPhuotAdapter;
import java.util.ArrayList;
import java.util.List;

public class PlaceActivity extends AppCompatActivity {
  RecyclerView recyclerplace;
  String category;
  DatabaseReference referenceplace;
  final List<DiemPhuot> diemPhuotList = new ArrayList<>();
  DiemPhuotAdapter adapter;
  private ProgressDialog mProgressDialog;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_place);

    Intent i = getIntent();
    category = i.getStringExtra("category");
    referenceplace = FirebaseDatabase.getInstance().getReference();

    recyclerplace = (RecyclerView) findViewById(R.id.recyclerplace);
    adapter = new DiemPhuotAdapter(diemPhuotList, this);
    LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
    recyclerplace.setHasFixedSize(true);
    recyclerplace.setLayoutManager(mLayoutManager);
    recyclerplace.setItemAnimator(new DefaultItemAnimator());
    recyclerplace.setAdapter(adapter);
    showProgressDialog();
    loadData();
  }

  private void loadData() {
    referenceplace.child("DiaDiem").orderByChild("category")
        .equalTo(category)
        .addValueEventListener(new ValueEventListener() {
          @Override
          public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot child : dataSnapshot.getChildren()) {
              DiemPhuot diemPhuot = child.getValue(DiemPhuot.class);
              diemPhuotList.add(diemPhuot);
              adapter.notifyDataSetChanged();
            }
            hideProgressDialog();

          }

          @Override
          public void onCancelled(DatabaseError databaseError) {

          }
        });
  }
  private void showProgressDialog() {
    if (mProgressDialog == null) {
      mProgressDialog = new ProgressDialog(this);
      mProgressDialog.setMessage("Đang lấy dữ liệu chờ xíu !");
      mProgressDialog.setIndeterminate(true);
    }

    mProgressDialog.show();
  }

  private void hideProgressDialog() {
    if (mProgressDialog != null && mProgressDialog.isShowing()) {
      mProgressDialog.hide();
    }
  }

}
