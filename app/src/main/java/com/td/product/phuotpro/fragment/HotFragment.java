package com.td.product.phuotpro.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.td.product.phuotpro.DiemDen.Place;
import com.td.product.phuotpro.DiemDen.PlaceAdapter;
import com.td.product.phuotpro.Exp;
import com.td.product.phuotpro.Item;
import com.td.product.phuotpro.News;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.XMLDOMParser;
import com.td.product.phuotpro.activity.InfoActivity;
import com.td.product.phuotpro.activity.NewsActivity;
import com.td.product.phuotpro.activity.PlaceDetailActivity;
import com.td.product.phuotpro.adapter.ExpAdapter;
import com.td.product.phuotpro.adapter.ItemAdapter;
import com.td.product.phuotpro.adapter.NewsAdapter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.td.product.phuotpro.R.id.listplace;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotFragment extends Fragment {
    TextView txtplace, txtnews, txtexp, txtitem;
    RecyclerView PlacerecyclerView;
    PlaceAdapter placeAdapter;
    List<Place> listPlace;
    View v;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference, referenceexp, referenceitem;
    ProgressDialog progressDialog;
    ArrayList<String> listlink;
    List<News> newsList;
    RecyclerView recyclernews;
    NewsAdapter newsAdapter;
    String linkrss = "http://vnexpress.net/rss/du-lich.rss";
    List<Exp> expList;
    RecyclerView recyclerexp;
    ExpAdapter expAdapter;
    List<Item> itemList;
    RecyclerView recycleritem;
    ItemAdapter itemAdapter;

    public HotFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this com.td.product.phuotpro.fragment
        v = inflater.inflate(R.layout.fragment_hot, container, false);
        init();
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Lấy dữ liệu");
        progressDialog.setMessage("Đang lấy dữ liệu chờ xíu...");
        progressDialog.show();
        listPlace = new ArrayList<>();
        placeAdapter = new PlaceAdapter(listPlace, getActivity());
        PlacerecyclerView.setAdapter(placeAdapter);
        firebaseDatabase = FirebaseDatabase.getInstance();
        reference = firebaseDatabase.getReference().child("NoiBat").child("DiemDen");
        expList = new ArrayList<>();
        expAdapter = new ExpAdapter(expList, getActivity());
        recyclerexp.setAdapter(expAdapter);
        referenceexp = FirebaseDatabase.getInstance().getReference();
        itemList = new ArrayList<>();
        itemAdapter = new ItemAdapter(itemList, getActivity());
        recycleritem.setAdapter(itemAdapter);
        referenceitem = FirebaseDatabase.getInstance().getReference();
        loadData();
        placeAdapter.setOnItemClickListener(new PlaceAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Place place = listPlace.get(position);
                listlink = new ArrayList<String>();
                for (int i = 0; i < place.listimage.size(); i++) {
                    listlink.add(place.listimage.get(i));
                }
                Intent intent = new Intent(getContext(), PlaceDetailActivity.class);
                intent.putStringArrayListExtra("listlink", (ArrayList<String>) listlink);
                intent.putExtra("lat", place.latitude);
                intent.putExtra("lon", place.longtitude);
                intent.putExtra("info", place.info);
                intent.putExtra("note", place.note);
                intent.putExtra("route", place.route);
                intent.putExtra("placeid", place.placeid);
                intent.putExtra("title", place.title);
                startActivity(intent);

            }
        });
        expAdapter.setOnItemClickListener(new ExpAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Exp exp = expList.get(position);
                Intent i = new Intent(getActivity(), InfoActivity.class);
                i.putExtra("info", exp.info);
                startActivity(i);
            }
        });
        newsList = new ArrayList<News>();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new ReadXML().execute(linkrss);

            }
        });

        return v;
    }


    private void loadData() {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Place place = child.getValue(Place.class);
                    listPlace.add(place);
                    placeAdapter.notifyDataSetChanged();
                }
                txtplace.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        referenceexp.child("KinhNghiem").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Exp exp = child.getValue(Exp.class);
                    expList.add(exp);
                    expAdapter.notifyDataSetChanged();
                }
                txtexp.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        referenceitem.child("DoPhuot").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Item item = child.getValue(Item.class);
                    itemList.add(item);
                    itemAdapter.notifyDataSetChanged();
                }
                txtitem.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void init() {
        PlacerecyclerView = (RecyclerView) v.findViewById(listplace);
        PlacerecyclerView.setHasFixedSize(true);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        PlacerecyclerView.setLayoutManager(horizontalLayoutManagaer);
        recyclernews = (RecyclerView) v.findViewById(R.id.recyclernews);
        recyclernews.setHasFixedSize(true);
        LinearLayoutManager horizontalnews = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclernews.setLayoutManager(horizontalnews);
        recyclerexp = (RecyclerView) v.findViewById(R.id.recyclerexp);
        recyclerexp.setHasFixedSize(true);
        LinearLayoutManager horizontalexp = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerexp.setLayoutManager(horizontalexp);
        recycleritem = (RecyclerView) v.findViewById(R.id.recycleritem);
        recycleritem.setHasFixedSize(true);
        LinearLayoutManager horizontalitem = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recycleritem.setLayoutManager(horizontalitem);
        txtplace = (TextView) v.findViewById(R.id.txtplace);
        txtnews = (TextView) v.findViewById(R.id.txtnews);
        txtexp = (TextView) v.findViewById(R.id.txtexp);
        txtitem = (TextView) v.findViewById(R.id.txtitem);
    }

    public class ReadXML extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            return docNoiDung_Tu_URL(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (TextUtils.isEmpty(s)) {
                return;
            }
            XMLDOMParser parser = new XMLDOMParser();
            Document document = parser.getDocument(s);
            NodeList nodeList = document.getElementsByTagName("item");
            NodeList nodelistdes = document.getElementsByTagName("description");
            for (int i = 0; i < 10; i++) {
                News news = new News();
                String cdata = nodelistdes.item(i + 1).getTextContent();
                Pattern pattern = Pattern.compile("<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>");
                Matcher matcher = pattern.matcher(cdata);
                if (matcher.find()) {
                    news.setImagelink(matcher.group(1));
                }
                String des;
                int start = cdata.indexOf("</br>");
                int start2 = cdata.indexOf("r>");
                if (start >= 0) {
                    des = cdata.substring(start + 5);
                    news.setDecription(des);
                } else {
                    des = cdata.substring(start2 + 5);
                    news.setDecription(des);
                }
                Element element = (Element) nodeList.item(i);
                news.setTitle(parser.getValue(element, "title"));
                news.setLink(parser.getValue(element, "link"));
                if (des.equals("escription") == false) {
                    newsList.add(news);
                }
            }
            newsAdapter = new NewsAdapter(newsList, getActivity());
            recyclernews.setAdapter(newsAdapter);
            newsAdapter.notifyDataSetChanged();
            txtnews.setVisibility(View.VISIBLE);
            newsAdapter.setOnItemClickListener(new NewsAdapter.ClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    News news = newsList.get(position);
                    Intent i = new Intent(getActivity(), NewsActivity.class);
                    i.putExtra("link", news.getLink());
                    startActivity(i);
                }
            });
        }
    }

    private static String docNoiDung_Tu_URL(String theUrl) {
        StringBuilder content = new StringBuilder();

        try {
            // create a url object
            URL url = new URL(theUrl);

            // create a urlconnection object
            URLConnection urlConnection = url.openConnection();

            // wrap the urlconnection in a bufferedreader
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String line;

            // read from the urlconnection via the bufferedreader
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line + "\n");
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content.toString();
    }


}
