package com.td.product.phuotpro.phuongtien;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.R;

import java.util.List;

/**
 * Created by Admin on 4/12/2017.
 */

public class TransportAdapter extends RecyclerView.Adapter<TransportAdapter.ViewHolder>{
    private List<Transport> transportList;
    Context context;
    private static ClickListener clickListener;

    public TransportAdapter(List<Transport> transportList,Context context) {
        this.transportList=transportList;
        this.context=context;
    }

    @Override
    public TransportAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.phuongtien_item,parent,false));
    }

    @Override
    public void onBindViewHolder(TransportAdapter.ViewHolder holder, int position) {

        Transport transport=transportList.get(position);
        holder.txttitle.setText(transport.title);
        Picasso.with(context).load(transport.linkimage).into(holder.imgv);
    }

    @Override
    public int getItemCount() {
        return transportList.size();
    }
    public static  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txttitle;
        ImageView imgv;
        public ViewHolder(View itemView) {
            super(itemView);
            txttitle = (TextView) itemView.findViewById(R.id.txttitle);
            imgv=(ImageView)itemView.findViewById(R.id.imgv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        TransportAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

    }
}
