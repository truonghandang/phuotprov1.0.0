package com.td.product.phuotpro.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.td.product.phuotpro.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

public class NewsActivity extends AppCompatActivity {
    String link;
    WebView webView;
    ProgressBar progressBar;
    String detail="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        init();
        Intent i=getIntent();
        link=i.getStringExtra("link");
        new GetData().execute();
    }

    private void init() {
        webView=(WebView)findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setHorizontalScrollBarEnabled(false);
//        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
//        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
//        webView.getSettings().setAppCacheEnabled(true);
//        webView.setWebChromeClient(new WebChromeClient());
//        webView.getSettings().setAllowFileAccess(true);
//        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
//        webView.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);
//        webView.setWebViewClient(new WebViewClient());
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//        webView.getSettings().setSupportMultipleWindows(true);
//        webView.getSettings().setLoadWithOverviewMode(true);
        progressBar=(ProgressBar)findViewById(R.id.progressbar);
    }
    public class GetData extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Document doc= Jsoup.connect(link).get();
                Elements title = doc.select("h1");
                Elements date=doc.select("div.block_timer.left.txt_666");
                Elements description=doc.select("div.short_intro.txt_666");
                Elements main=doc.select("div.fck_detail.width_common");
                Elements main1=doc.select("div#article_content");
//                Elements author=doc.select("div.author_mail.width_common");
                detail += "<h3 style = \" color: #027000 \">" + title.text()
                        + "</h3>";
                detail += "<font size=\" 2em \" style = \" color: #666 \"><em>"
                        + date.text() + "</em></font>";
                detail += "<p style = \" color: #444 \"><b>" + "<font size=\" 4em \" >"
                        + description.text() + "</font></b></p>";
                detail+="<font size=\" 4em \" >"+ main1.toString() + "</font>";
                detail += "<font size=\" 4em \" >"+  main.toString() + "</font>";
//                detail+="<p style=\"text-align: right;\" ><strong>"+author.toString()+"</strong></p>";
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
           progressBar.setVisibility(View.GONE);
            webView.loadDataWithBaseURL(
                    "",
                    "<style>img{display: inline;height: auto;max-width: 100%;}"
                            +"iframe{max-width: 100%;height:auto;}"
                            + " p {font-family:\"Tangerine\", \"Sans-serif\",  \"Serif\" font-size: 48px} </style>"
                            + detail, "text/html; video/mpeg", "UTF-8","");
        }
    }
}

