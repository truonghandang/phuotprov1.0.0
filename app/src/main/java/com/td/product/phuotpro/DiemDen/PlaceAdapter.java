package com.td.product.phuotpro.DiemDen;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.R;

import java.util.List;

/**
 * Created by Admin on 4/2/2017.
 */

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.ViewHolder>{
    private List<Place> listPlace;
    Context context;
    private static ClickListener clickListener;

    public PlaceAdapter(List<Place> listPlace,Context context) {
        this.listPlace=listPlace;
        this.context=context;
    }

    @Override
    public PlaceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.place_item,parent,false));
    }

    @Override
    public void onBindViewHolder(PlaceAdapter.ViewHolder holder, int position) {

        Place place=listPlace.get(position);
        holder.txttitle.setText(place.title);
        Picasso.with(context).load(place.imagelink).into(holder.imgv);
        holder.txtdes.setText(place.description);


    }

    @Override
    public int getItemCount() {
        return listPlace.size();
    }
    public static  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txttitle,txtdes;
        ImageView imgv;
        public ViewHolder(View itemView) {
            super(itemView);
            txttitle = (TextView) itemView.findViewById(R.id.txttitle);
            txtdes = (TextView) itemView.findViewById(R.id.txtdes);
            imgv=(ImageView)itemView.findViewById(R.id.imgv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        PlaceAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

    }
}
