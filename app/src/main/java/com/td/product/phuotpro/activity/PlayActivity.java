package com.td.product.phuotpro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.td.product.phuotpro.DiaDiemNho;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.adapter.PlayAdapter;

import java.util.ArrayList;
import java.util.List;

public class PlayActivity extends AppCompatActivity {
    String placeid;
    RecyclerView recyclerplay;
    PlayAdapter playAdapter;
    DatabaseReference databaseReference;
    List<DiaDiemNho> diaDiemNhoList;
    ArrayList<String>listlink;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        Intent i=getIntent();
        placeid=i.getStringExtra("placeid");
        Log.d("placeid",placeid);
        init();
        diaDiemNhoList=new ArrayList<>();
        playAdapter=new PlayAdapter(diaDiemNhoList,getApplication());
        recyclerplay.setAdapter(playAdapter);
        databaseReference= FirebaseDatabase.getInstance().getReference();
        databaseReference.child("DiaDiemNho").orderByChild("placeid").equalTo(placeid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                diaDiemNhoList.clear();
                for(DataSnapshot child:dataSnapshot.getChildren()){
                    DiaDiemNho diaDiemNho=child.getValue(DiaDiemNho.class);
                    diaDiemNhoList.add(diaDiemNho);
                    playAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        playAdapter.setOnItemClickListener(new PlayAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                DiaDiemNho diaDiemNho=diaDiemNhoList.get(position);
                listlink=new ArrayList<String>();
                for(int i=0;i<diaDiemNho.listimage.size();i++){
                    listlink.add(diaDiemNho.listimage.get(i));
                }
                Intent intent=new Intent(getApplication(),PlayDetailActivity.class);
                intent.putStringArrayListExtra("listlink", (ArrayList<String>) listlink);
                intent.putExtra("info",diaDiemNho.info);
                intent.putExtra("address",diaDiemNho.address);
                intent.putExtra("title",diaDiemNho.title);
                Log.d("title",diaDiemNho.title);
                intent.putExtra("hotelid",diaDiemNho.hotelid);
                intent.putExtra("lat",diaDiemNho.latitude);
                intent.putExtra("lon",diaDiemNho.longtitude);
                startActivity(intent);

            }
        });
    }

    private void init() {
        recyclerplay=(RecyclerView)findViewById(R.id.recyclerplay);
        recyclerplay.setHasFixedSize(true);
        recyclerplay.setLayoutManager(new LinearLayoutManager(getApplication()));
    }
}
