package com.td.product.phuotpro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.td.product.phuotpro.DiemDen.MenuPlace;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.adapter.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class PlaceDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private static int currentpage = 0;
    private static int numpages = 0;
    private ArrayList<String> listlink;
    private static final String[] title = {"Giới thiệu", "Phương tiện", "Nhà nghỉ, khách sạn", "Địa điểm vui chơi", "Ẩm thực", "Lịch trình và chi phí", "Thời tiết", "Lưu ý"};
    private static final Integer[] img = {R.drawable.ic_info_black_24dp, R.drawable.ic_motorcycle_black_24dp, R.drawable.ic_hotel_black_24dp, R.drawable.ic_place_black_24dp, R.drawable.ic_restaurant_menu_black_24dp, R.drawable.ic_playlist_add_check_black_24dp, R.drawable.ic_wb_cloudy_black_24dp, R.drawable.ic_note_black_24dp};
    private List<MenuPlace> menuPlaces;
    private String info, note, route, placeid, place;
    private double lat, lon;

    private RelativeLayout rlInfo;
    private RelativeLayout rlTravel;
    private RelativeLayout rlHotel;
    private RelativeLayout rlLocation;
    private RelativeLayout rlFood;
    private RelativeLayout rlSchedule;
    private RelativeLayout rlWeather;
    private RelativeLayout rlNote;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_detail);
        viewPager = (ViewPager) findViewById(R.id.pager);

        rlInfo = (RelativeLayout) findViewById(R.id.rl_info);
        rlTravel = (RelativeLayout) findViewById(R.id.rl_travel);
        rlHotel = (RelativeLayout) findViewById(R.id.rl_hotel);
        rlLocation = (RelativeLayout) findViewById(R.id.rl_location);
        rlFood = (RelativeLayout) findViewById(R.id.rl_food);
        rlSchedule = (RelativeLayout) findViewById(R.id.rl_schedule);
        rlWeather = (RelativeLayout) findViewById(R.id.rl_weather);
        rlNote = (RelativeLayout) findViewById(R.id.rl_note);

        rlInfo.setOnClickListener(this);
        rlTravel.setOnClickListener(this);
        rlHotel.setOnClickListener(this);
        rlLocation.setOnClickListener(this);
        rlFood.setOnClickListener(this);
        rlSchedule.setOnClickListener(this);
        rlWeather.setOnClickListener(this);
        rlNote.setOnClickListener(this);

        listlink = new ArrayList<String>();
        Intent i = getIntent();
        listlink = i.getStringArrayListExtra("listlink");
        info = i.getStringExtra("info");
        note = i.getStringExtra("note");
        route = i.getStringExtra("route");
        placeid = i.getStringExtra("placeid");
        place = i.getStringExtra("place");
        lat = i.getDoubleExtra("lat", 9);
        lon = i.getDoubleExtra("lon", 9);
        Log.d("listlink1", String.valueOf(listlink));
        adapter = new ViewPagerAdapter(this, listlink);
        viewPager.setAdapter(adapter);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentpage = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    int pagecount = listlink.size();
                    if (currentpage == 0) {
                        viewPager.setCurrentItem(pagecount, false);
                    } else if (currentpage == pagecount - 1) {
                        viewPager.setCurrentItem(0, false);
                    }
                }
            }
        });
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            @Override
            public void run() {
                if (currentpage == numpages) {
                    currentpage = 0;
                }
                viewPager.setCurrentItem(currentpage++, true);
            }
        };
        Timer swipe = new Timer();
        swipe.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);
            }
        }, 2000, 2000);
        menuPlaces = new ArrayList<>();
        for (int j = 0; j < title.length; j++) {
            menuPlaces.add(new MenuPlace(title[j], img[j]));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.rl_info):
                Intent intentInfo = new Intent(getApplication(), InfoActivity.class);
                intentInfo.putExtra("info", info);
                startActivity(intentInfo);
                break;
            case (R.id.rl_travel):
                Intent intentTravel = new Intent(getApplication(), PhuongTienActivity.class);
                startActivity(intentTravel);
                break;
            case (R.id.rl_hotel):
                Intent intentHotel = new Intent(getApplication(), HotelActivity.class);
                startActivity(intentHotel);
                break;
            case (R.id.rl_location):
                Intent intentPlay = new Intent(getApplication(), PlayActivity.class);
                intentPlay.putExtra("placeid", placeid);
                startActivity(intentPlay);
                break;
            case (R.id.rl_food):
                Intent intentFood = new Intent(getApplication(), AmThucActivity.class);
                intentFood.putExtra("placeid", placeid);
                startActivity(intentFood);
                break;
            case (R.id.rl_schedule):
                Intent intentSchedule = new Intent(getApplication(), RouteActivity.class);
                intentSchedule.putExtra("route", route);
                startActivity(intentSchedule);
                break;
            case (R.id.rl_weather):
                Intent intentWeather = new Intent(getApplication(), WeatherActivity.class);
                intentWeather.putExtra("title", place);
                intentWeather.putExtra("lat", lat);
                intentWeather.putExtra("lon", lon);
                intentWeather.putStringArrayListExtra("listlink", (ArrayList<String>) listlink);
                startActivity(intentWeather);
                break;
            case (R.id.rl_note):
                Intent intentNote = new Intent(getApplication(), NoteActivity.class);
                intentNote.putExtra("note", note);
                startActivity(intentNote);
                break;
        }
    }
}
