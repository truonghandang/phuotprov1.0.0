package com.td.product.phuotpro;

/**
 * Created by Admin on 5/11/2017.
 */

public class Exp {
    public String title;
    public String imagelink;
    public String info;
    public String description;

    public Exp(){

    }
    public Exp(String title,String imagelink,String info,String description){
        this.title=title;
        this.imagelink=imagelink;
        this.info=info;
        this.description=description;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImagelink() {
        return imagelink;
    }

    public void setImagelink(String imagelink) {
        this.imagelink = imagelink;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
