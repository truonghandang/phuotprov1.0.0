package com.td.product.phuotpro;

import java.util.ArrayList;

/**
 * Created by Admin on 4/28/2017.
 */

public class NhaHang {
    public String address;
    public String cost;
    public String hotelid;
    public String imagelink;
    public String info;
    public double latitude;
    public ArrayList<String> listimage;
    public double longtitude;
    public String phone;
    public String placeid;
    public String time;
    public String title;

    public NhaHang(){

    }
    public NhaHang(String address,String cost,String hotelid,String imagelink,String info,double latitude,ArrayList<String>listimage,double longtitude,String phone,String placeid,String time,String title){
        this.address=address;
        this.cost=cost;
        this.hotelid=hotelid;
        this.imagelink=imagelink;
        this.info=info;
        this.latitude=latitude;
        this.listimage=listimage;
        this.longtitude=longtitude;
        this.phone=phone;
        this.placeid=placeid;
        this.time=time;
        this.title=title;

    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getHotelid() {
        return hotelid;
    }

    public void setHotelid(String hotelid) {
        this.hotelid = hotelid;
    }

    public String getImagelink() {
        return imagelink;
    }

    public void setImagelink(String imagelink) {
        this.imagelink = imagelink;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public ArrayList<String> getListimage() {
        return listimage;
    }

    public void setListimage(ArrayList<String> listimage) {
        this.listimage = listimage;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPlaceid() {
        return placeid;
    }

    public void setPlaceid(String placeid) {
        this.placeid = placeid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
