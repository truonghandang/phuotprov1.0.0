package com.td.product.phuotpro.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.Weather;
import com.td.product.phuotpro.adapter.WeatherAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class WeatherActivity extends AppCompatActivity {
    ImageView iconweather,imgv;
    TextView txttemp,txttempmax,txttempmin,txtdes,txttemp1,txtpressure,txthumidity,txtwind;
    ArrayList<String> listlink;
    String title,urlcurrentweather,urlforecast;
    double lat,lon;
    RelativeLayout relativeLayout;
    RecyclerView recyclerforecast;
    WeatherAdapter weatherAdapter;
    List<Weather> weatherList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        listlink=new ArrayList<String>();
        Intent i = getIntent();
        listlink = i.getStringArrayListExtra("listlink");
        title=i.getStringExtra("title");
        lat=i.getDoubleExtra("lat",9);
        lon=i.getDoubleExtra("lon",9);
        WindowManager w = getWindowManager();
        Display d = w.getDefaultDisplay();
        urlcurrentweather="http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&units=metric&lang=vi&appid=88537f8bef5eeff0c4a99eb06406ab35";
        urlforecast="http://api.openweathermap.org/data/2.5/forecast/daily?lat="+lat+"&lon="+lon+"&units=metric&cnt=7&lang=vi&appid=88537f8bef5eeff0c4a99eb06406ab35";
        relativeLayout=(RelativeLayout)findViewById(R.id.relative);
        txtdes=(TextView)findViewById(R.id.txtdes);
        txttemp=(TextView)findViewById(R.id.txttemp);
        txttempmax=(TextView)findViewById(R.id.txttempmax);
        txttempmin=(TextView)findViewById(R.id.txttempmin);
        txttemp1=(TextView)findViewById(R.id.txttemp1);
        txtpressure=(TextView)findViewById(R.id.txtpressure);
        txthumidity=(TextView)findViewById(R.id.txthumidity);
        txtwind=(TextView)findViewById(R.id.txtwind);
        imgv=(ImageView)findViewById(R.id.imgv);
        Picasso.with(getApplication()).load("https://cdn3.ivivu.com/2016/07/quy-nhon-ivivu-1.jpg").resize(d.getWidth(),d.getHeight()).centerCrop().into(target);
        iconweather=(ImageView)findViewById(R.id.iconweather);
        recyclerforecast=(RecyclerView)findViewById(R.id.recyclerforecast);
        recyclerforecast.setHasFixedSize(true);
        recyclerforecast.setLayoutManager(new LinearLayoutManager(getApplication()));
        weatherList=new ArrayList<>();
        Calendar calendar=Calendar.getInstance();
        int currenthour=calendar.get(Calendar.HOUR);
        Log.d("currenthour", String.valueOf(currenthour));
        //Curent Weather
        final JsonObjectRequest objcurrentweather = new JsonObjectRequest(Request.Method.GET,
                urlcurrentweather, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray weather=response.getJSONArray("weather");
                    JSONObject oweather=weather.getJSONObject(0);
                    String id=oweather.getString("id");
                    if(id=="900"){
                        iconweather.setBackgroundResource(R.drawable.wtornado);
                    }
                    if(id=="901" || id=="902"){
                        iconweather.setBackgroundResource(R.drawable.wtropical_storm);
                    }
                    if(id=="903"){
                        iconweather.setBackgroundResource(R.drawable.wcold);
                    }
                    if(id=="904"){
                        iconweather.setBackgroundResource(R.drawable.whot);
                    }
                    if(id=="905"){
                        iconweather.setBackgroundResource(R.drawable.wwind);
                    }
                    if(id=="906"){
                        iconweather.setBackgroundResource(R.drawable.whail);
                    }
                    if(id=="951"){
                        Calendar calendar=Calendar.getInstance();
                        int currenthour=calendar.get(Calendar.HOUR);
                        if(currenthour>=6 && currenthour<=18){
                            iconweather.setBackgroundResource(R.drawable.w1d);
                        }
                        else {
                            iconweather.setBackgroundResource(R.drawable.w1n);
                        }
                    }
                    if(id=="952" || id=="953"){
                        iconweather.setBackgroundResource(R.drawable.wwind);
                    }
                    if(id=="954" || id=="955"){
                        iconweather.setBackgroundResource(R.drawable.wwind);
                    }
                    if(id=="956" || id=="957"){
                        iconweather.setBackgroundResource(R.drawable.wwind);
                    }
                    if(id=="958"){
                        iconweather.setBackgroundResource(R.drawable.wwind);
                    }
                    if(id=="959" || id=="960"){
                        iconweather.setBackgroundResource(R.drawable.wtropical_storm);
                    }
                    if(id=="961" || id=="962"){
                        iconweather.setBackgroundResource(R.drawable.wtropical_storm);
                    }
                    else {
                        WeatherCondition(oweather.getString("icon"));
                    }
                    String condition=oweather.getString("description").substring(0,1).toUpperCase()+oweather.getString("description").substring(1);
                    txtdes.setText(condition);
                    JSONObject main=response.getJSONObject("main");
                    txttemp.setText(String.valueOf(Math.round(main.getDouble("temp")))+"°");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplication());
        requestQueue.add(objcurrentweather);
        //ForeCast Weather
        JsonObjectRequest objforecast=new JsonObjectRequest(Request.Method.GET,
                urlforecast, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray listforecast=response.getJSONArray("list");
                    JSONObject object=listforecast.getJSONObject(0);
                    txtpressure.setText(String.valueOf(object.getDouble("pressure"))+" hPa");
                    txthumidity.setText(String.valueOf(Math.round(object.getDouble("humidity")))+" %");
                    txtwind.setText(String.valueOf(object.getDouble("speed")) +" m/s");
                    JSONObject otemp=object.getJSONObject("temp");
                    txttemp1.setText(String.valueOf(Math.round(otemp.getDouble("day")))+" °C");
                    txttempmax.setText("↑ "+String.valueOf(Math.round(otemp.getDouble("max")))+" °");
                    txttempmin.setText("↓ "+String.valueOf(Math.round(otemp.getDouble("min")))+" °");
                    JSONArray aweather=object.getJSONArray("weather");
                    JSONObject oweather=aweather.getJSONObject(0);
                    String id=oweather.getString("id");
                    if(id=="900"){
                        iconweather.setBackgroundResource(R.drawable.wtornado);
                    }
                    if(id=="901" || id=="902"){
                        iconweather.setBackgroundResource(R.drawable.wtropical_storm);
                    }
                    if(id=="903"){
                        iconweather.setBackgroundResource(R.drawable.wcold);
                    }
                    if(id=="904"){
                        iconweather.setBackgroundResource(R.drawable.whot);
                    }
                    if(id=="905"){
                        iconweather.setBackgroundResource(R.drawable.wwind);
                    }
                    if(id=="906"){
                        iconweather.setBackgroundResource(R.drawable.whail);
                    }
                    if(id=="951"){
                        Calendar calendar=Calendar.getInstance();
                        int currenthour=calendar.get(Calendar.HOUR);
                        if(currenthour>=6 && currenthour<=18){
                            iconweather.setBackgroundResource(R.drawable.w1d);
                        }
                        else {
                            iconweather.setBackgroundResource(R.drawable.w1n);
                        }
                    }
                    if(id=="952" || id=="953"){
                        iconweather.setBackgroundResource(R.drawable.wwind);
                    }
                    if(id=="954" || id=="955"){
                        iconweather.setBackgroundResource(R.drawable.wwind);
                    }
                    if(id=="956" || id=="957"){
                        iconweather.setBackgroundResource(R.drawable.wwind);
                    }
                    if(id=="958"){
                        iconweather.setBackgroundResource(R.drawable.wwind);
                    }
                    if(id=="959" || id=="960"){
                        iconweather.setBackgroundResource(R.drawable.wtropical_storm);
                    }
                    if(id=="961" || id=="962"){
                        iconweather.setBackgroundResource(R.drawable.wtropical_storm);
                    }
                    else {
                        WeatherCondition(oweather.getString("icon"));
                    }
//                    WeatherCondition(oweather.getString("icon"));
//                    icon2=oweather.getString("icon");
//                    imgv.setBackgroundResource(R.drawable.o4d);
                    for(int i=0;i<listforecast.length();i++){
                        JSONObject objforecast=listforecast.getJSONObject(i);
                        JSONObject objtemp=objforecast.getJSONObject("temp");
                        double tempmax,tempmin;
                        tempmax=objtemp.getDouble("max");
                        Log.d("max", String.valueOf(tempmax));
                        tempmin=objtemp.getDouble("min");
                        JSONArray arrweather=objforecast.getJSONArray("weather");
                        JSONObject objweather=arrweather.getJSONObject(0);
//                        icon4=objweather.getString("icon");
//                        WeatherCondition2(objweather.getString("description"));
                        Weather weather=new Weather();
                        weather.setIcon(objweather.getString("icon"));
                        weather.setId(objweather.getString("id"));
                        weather.setTempmax(tempmax);
                        weather.setTempmin(tempmin);
                        weatherList.add(weather);

                    }
                    weatherAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        RequestQueue requestQueue1 = Volley.newRequestQueue(getApplication());
        requestQueue1.add(objforecast);
        weatherAdapter=new WeatherAdapter(getApplication(),weatherList);
        recyclerforecast.setAdapter(weatherAdapter);
    }

    private void WeatherCondition(String icon) {
        switch(icon){
            case "01d":
                iconweather.setBackgroundResource(R.drawable.w1d);
                imgv.setBackgroundResource(R.drawable.w1d);
                break;
            case "01n":
                iconweather.setBackgroundResource(R.drawable.w1n);
                imgv.setBackgroundResource(R.drawable.w1n);
                break;
            case "02d":
                iconweather.setBackgroundResource(R.drawable.w2d);
                imgv.setBackgroundResource(R.drawable.w2d);
                break;
            case "02n":
                iconweather.setBackgroundResource(R.drawable.w2n);
                imgv.setBackgroundResource(R.drawable.w2n);
                break;
            case "03d":
                iconweather.setBackgroundResource(R.drawable.w3d);
                imgv.setBackgroundResource(R.drawable.w3d);
                break;
            case "03n":
                iconweather.setBackgroundResource(R.drawable.w3n);
                imgv.setBackgroundResource(R.drawable.w3n);
                break;
            case "04d":
                iconweather.setBackgroundResource(R.drawable.w4d);
                imgv.setBackgroundResource(R.drawable.w4d);
                break;
            case "04n":
                iconweather.setBackgroundResource(R.drawable.w4n);
                imgv.setBackgroundResource(R.drawable.w4n);
                break;
            case "09d":
                iconweather.setBackgroundResource(R.drawable.w9d);
                imgv.setBackgroundResource(R.drawable.w9d);
                break;
            case "09n":
                iconweather.setBackgroundResource(R.drawable.w9n);
                imgv.setBackgroundResource(R.drawable.w9n);
                break;
            case "10d":
                iconweather.setBackgroundResource(R.drawable.w10d);
                imgv.setBackgroundResource(R.drawable.w10d);
                break;
            case "10n":
                iconweather.setBackgroundResource(R.drawable.w10n);
                imgv.setBackgroundResource(R.drawable.w10n);
                break;
            case "11d":
                iconweather.setBackgroundResource(R.drawable.w11d);
                imgv.setBackgroundResource(R.drawable.w11d);
                break;
            case "11n":
                iconweather.setBackgroundResource(R.drawable.w11n);
                imgv.setBackgroundResource(R.drawable.w11n);
                break;
            case "13d":
                iconweather.setBackgroundResource(R.drawable.w13d);
                imgv.setBackgroundResource(R.drawable.w13d);
                break;
            case "13n":
                iconweather.setBackgroundResource(R.drawable.w13n);
                imgv.setBackgroundResource(R.drawable.w13n);
                break;
            case "50d":
                iconweather.setBackgroundResource(R.drawable.w10d);
                imgv.setBackgroundResource(R.drawable.w10d);
                break;
            case "50n":
                iconweather.setBackgroundResource(R.drawable.w10n);
                imgv.setBackgroundResource(R.drawable.w10n);
                break;
            default:
                Picasso.with(getApplication()).load("http://openweathermap.org/img/w/"+icon+".png").into(iconweather);
                Picasso.with(getApplication()).load("http://openweathermap.org/img/w/"+icon+".png").into(imgv);
                break;
        }

    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            Drawable drawable = new BitmapDrawable(getResources(), bitmap);
            relativeLayout.getRootView().setBackgroundDrawable(drawable);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };
}
