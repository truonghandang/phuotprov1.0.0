package com.td.product.phuotpro.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.td.product.phuotpro.Item;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.adapter.ItemAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ItemFragment extends Fragment {
    View v;
    RecyclerView recyclerhat,recyclerclothes;
    DatabaseReference referencehat,referenceclothes;
    ItemAdapter itemAdapter,itemAdapter1;
    List<Item> itemList,itemList1;


    public ItemFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this com.td.product.phuotpro.fragment
        v= inflater.inflate(R.layout.fragment_item, container, false);
        init();
        itemList=new ArrayList<>();
        itemAdapter=new ItemAdapter(itemList,getActivity());
        recyclerhat.setAdapter(itemAdapter);
        referencehat= FirebaseDatabase.getInstance().getReference();
        itemList1=new ArrayList<>();
        itemAdapter1=new ItemAdapter(itemList1,getActivity());
        recyclerclothes.setAdapter(itemAdapter1);
        referenceclothes= FirebaseDatabase.getInstance().getReference();
        loadData();
        return  v;
    }

    private void loadData() {
        referencehat.child("DoPhuot").orderByChild("category").equalTo("Khan").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot child:dataSnapshot.getChildren()){
                    Item item=child.getValue(Item.class);
                    itemList.add(item);
                    itemAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        referenceclothes.child("DoPhuot").orderByChild("category").equalTo("QuanAo").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot child:dataSnapshot.getChildren()){
                    Item item=child.getValue(Item.class);
                    itemList1.add(item);
                    itemAdapter1.notifyDataSetChanged();
                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void init() {
        recyclerhat=(RecyclerView)v.findViewById(R.id.recyclerhat);
        recyclerhat.setHasFixedSize(true);
        LinearLayoutManager horizontalhat = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerhat.setLayoutManager(horizontalhat);
        recyclerclothes=(RecyclerView)v.findViewById(R.id.recyclerclothes);
        recyclerclothes.setHasFixedSize(true);
        LinearLayoutManager horizontalclothes = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerclothes.setLayoutManager(horizontalclothes);

    }

}
