package com.td.product.phuotpro.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.td.product.phuotpro.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhuongtiendilaiFragment extends Fragment {
    WebView webView;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference;


    public PhuongtiendilaiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_phuongtiendilai, container, false);
        webView=(WebView)v.findViewById(R.id.webview);
        firebaseDatabase=FirebaseDatabase.getInstance();
        reference=firebaseDatabase.getReference().child("PhuongTienDiLai").child("BinhDinh");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                webView.loadDataWithBaseURL("",dataSnapshot.getValue(String.class).toString(), "text/html", "UTF-8", "");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return v;
    }

}
