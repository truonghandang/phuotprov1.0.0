package com.td.product.phuotpro.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.td.product.phuotpro.NhaHang;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.activity.NhaHangDetailActivity;
import com.td.product.phuotpro.adapter.OdauAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ODauFragment extends Fragment {
    RecyclerView recyclerodau;
    OdauAdapter odauAdapter;
    String placeid;
    ArrayList<String> listlink;
    ProgressDialog progressDialog;
    List<NhaHang> nhaHangList;
    DatabaseReference databaseReference;

    public ODauFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_odau, container, false);
        Intent i=getActivity().getIntent();
        placeid=i.getStringExtra("placeid");
        recyclerodau=(RecyclerView)v.findViewById(R.id.recyclerodau);
        recyclerodau.setHasFixedSize(true);
        recyclerodau.setLayoutManager(new GridLayoutManager(getActivity(),2));
        nhaHangList=new ArrayList<>();
        odauAdapter=new OdauAdapter(getActivity(),nhaHangList);
        recyclerodau.setAdapter(odauAdapter);
        databaseReference= FirebaseDatabase.getInstance().getReference();
        databaseReference.child("ODau").orderByChild("placeid").equalTo(placeid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                nhaHangList.clear();
                for(DataSnapshot child:dataSnapshot.getChildren()){
                    NhaHang  nhaHang=child.getValue(NhaHang.class);
                    nhaHangList.add(nhaHang);
                    Log.d("imagelink",nhaHang.imagelink);
                    odauAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        odauAdapter.setOnItemClickListener(new OdauAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                NhaHang nhaHang=nhaHangList.get(position);
                listlink=new ArrayList<String>();
                for(int i=0;i<nhaHang.listimage.size();i++){
                    listlink.add(nhaHang.listimage.get(i));
                }
                Intent i=new Intent(getActivity(),NhaHangDetailActivity.class);
                i.putExtra("info",nhaHang.info);
                i.putExtra("title",nhaHang.title);
                i.putExtra("phone",nhaHang.phone);
                i.putExtra("address",nhaHang.address);
                i.putExtra("time",nhaHang.time);
                i.putExtra("hotelid",nhaHang.hotelid);
                i.putExtra("lat",nhaHang.latitude);
                i.putExtra("lon",nhaHang.longtitude);
                i.putExtra("cost",nhaHang.cost);
                i.putStringArrayListExtra("listlink", (ArrayList<String>) listlink);
                startActivity(i);
            }
        });
        return v;
    }

}
