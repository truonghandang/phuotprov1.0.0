package com.td.product.phuotpro.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.td.product.phuotpro.Comment;
import com.td.product.phuotpro.R;

import java.util.List;

/**
 * Created by Admin on 4/21/2017.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder>{


    private List<Comment> commentList;
    Context context;
    private static ClickListener clickListener;

    public ReviewAdapter(List<Comment> commentList,Context context) {
        this.commentList=commentList;
        this.context=context;
    }

    @Override
    public ReviewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.review_hotel_item,parent,false));
    }

    @Override
    public void onBindViewHolder(ReviewAdapter.ViewHolder holder, int position) {
        Comment comment=commentList.get(position);
        holder.txtuser.setText(comment.name);
        Picasso.with(context).load(comment.imagelink).into(holder.imgv);
        holder.ratingBar.setRating(comment.rating);
        holder.txtreview.setText(comment.review);
        holder.txtdate.setText(comment.date);

    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }
    public static  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtuser,txtreview,txtdate;
        ImageView imgv;
        RatingBar ratingBar;
        public ViewHolder(View itemView) {
            super(itemView);
            txtuser = (TextView) itemView.findViewById(R.id.txtuser);
            imgv=(ImageView)itemView.findViewById(R.id.imgv);
            ratingBar=(RatingBar)itemView.findViewById(R.id.ratingBar);
            txtdate=(TextView)itemView.findViewById(R.id.txtdate);
            txtreview=(TextView)itemView.findViewById(R.id.txtreview) ;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        ReviewAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

    }
}
