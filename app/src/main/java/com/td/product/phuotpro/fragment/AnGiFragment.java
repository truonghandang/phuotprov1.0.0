package com.td.product.phuotpro.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.td.product.phuotpro.DiemDen.Place;
import com.td.product.phuotpro.R;
import com.td.product.phuotpro.activity.DacSanDetailActivity;
import com.td.product.phuotpro.adapter.AnGiAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnGiFragment extends Fragment {
    List<Place> placeList;
    RecyclerView recyclerangi;
    AnGiAdapter anGiAdapter;
    DatabaseReference databaseReference;
    String placeid;
    ArrayList<String>listlink;
    ProgressDialog progressDialog;

    public AnGiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_an_gi, container, false);
        Intent i=getActivity().getIntent();
        placeid=i.getStringExtra("placeid");
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setTitle("Tải dữ liệu");
        progressDialog.setMessage("Đang tải chờ xíu ...!");
        progressDialog.show();
        recyclerangi=(RecyclerView)v.findViewById(R.id.recyclerangi);
        recyclerangi.setHasFixedSize(true);
        recyclerangi.setLayoutManager(new LinearLayoutManager(getActivity()));
        placeList=new ArrayList<>();
        anGiAdapter=new AnGiAdapter(getActivity(),placeList);
        recyclerangi.setAdapter(anGiAdapter);
        databaseReference= FirebaseDatabase.getInstance().getReference();
        databaseReference.child("Angi").orderByChild("placeid").equalTo(placeid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                placeList.clear();
                for(DataSnapshot child:dataSnapshot.getChildren()){
                    Place  place=child.getValue(Place.class);
                    Log.d("title",place.title);
                    placeList.add(place);
                    anGiAdapter.notifyDataSetChanged();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        anGiAdapter.setOnItemClickListener(new AnGiAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Place place=placeList.get(position);
                listlink=new ArrayList<String>();
                for(int i=0;i<place.listimage.size();i++){
                    listlink.add(place.listimage.get(i));
                }
                Intent i=new Intent(getActivity(),DacSanDetailActivity.class);
                i.putExtra("info",place.info);
                i.putExtra("title",place.title);
                i.putStringArrayListExtra("listlink", (ArrayList<String>) listlink);
                startActivity(i);
            }
        });
        return  v;
    }

}
